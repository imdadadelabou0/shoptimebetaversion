import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shoptime/Events/orderEvent.dart';
import 'package:shoptime/models/orderProduct.dart';

class OrderBloc extends Bloc<OrderEvent, List<OrderProduct>> {
  OrderBloc(List<OrderProduct> initialState) : super(initialState);

  @override
  List<OrderProduct> get initialState => List<OrderProduct>();

  @override
  Stream<List<OrderProduct>> mapEventToState(OrderEvent event) async* {
    switch (event.eventType) {
      case EventType.add:
        List<OrderProduct> newState = List.from(state);
        if (event.orderProduct != null) {
          newState.add(event.orderProduct);
        }
        yield null;
        break;
      case EventType.delete:
        List<OrderProduct> newState = List.from(state);
        newState.remove(event.orderIndex);
        yield null;
        break;
      default:
        throw Exception('Event not found $event');
    }
  }
}
