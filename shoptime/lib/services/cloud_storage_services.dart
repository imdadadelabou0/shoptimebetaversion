import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class CloudStorageService {
  Future<CloudStorageResult> uploadImg(
      {@required File imageProduct, @required String id}) async {
    var imageFileName = DateTime.now().millisecondsSinceEpoch.toString();

    final StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(imageFileName);

    StorageUploadTask uploadTask = firebaseStorageRef.putFile(imageProduct);

    StorageTaskSnapshot storageSnapshot = await uploadTask.onComplete;

    var downloadUrl = await storageSnapshot.ref.getDownloadURL();

    if (uploadTask.isComplete) {
      var urlProduct = downloadUrl.toString();
      return CloudStorageResult(imgUrlProduct: urlProduct, id: imageFileName);
    }
    return null;
  }
}

class CloudStorageResult {
  final String imgUrlProduct;
  final String id;

  CloudStorageResult({this.imgUrlProduct, this.id});
}
