import 'dart:collection';
import 'package:flutter/foundation.dart';
import 'package:shoptime/models/product.dart';

class OrderModel extends ChangeNotifier {
  final List<Product> _orderList = [];

  UnmodifiableListView<Product> get orderList =>
      UnmodifiableListView(_orderList);

  void addInListOrder(Product product) {
    _orderList.add(product);
    notifyListeners();
  }
}
