import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shoptime/models/User.dart';

class StockUserData {
  final FirebaseFirestore _instanceStock;

  StockUserData({FirebaseFirestore instanceStock})
      : _instanceStock = instanceStock ?? FirebaseFirestore.instance;

  Future<void> addUserData(Users userData) {
    final CollectionReference collectionReference =
        _instanceStock.collection("users");
    final generatedId = collectionReference.doc().id;
    return collectionReference.doc(generatedId).set(
          userData.userToJson(),
          SetOptions(merge: true),
        );
  }

  Stream<List<Users>> getListUsers() {
    final CollectionReference collectionReference =
        _instanceStock.collection("users");
    return collectionReference.orderBy('username').snapshots().map(
          (snap) =>
              snap.docs.map((e) => Users().fromJsonToUser(e.data())).toList(),
        );
  }
}
