import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shoptime/models/product.dart';

class StoreBase {
  final FirebaseFirestore _instanceStore;

  StoreBase({FirebaseFirestore instanceStore})
      : _instanceStore = instanceStore ?? FirebaseFirestore.instance;

  Future<void> addProduct(Product product) async {
    final CollectionReference collectionReference =
        _instanceStore.collection("store");
    final String generatedID = collectionReference.doc().id;
    return collectionReference.doc(generatedID).set(
        product.copyWith(id: generatedID).toJson(), SetOptions(merge: true));
  }

  Future<List<Product>> getProducts() async {
    final CollectionReference collectionReference =
        _instanceStore.collection("store");
    return collectionReference.get().then((value) {
      final products = value.docs.map((json) {
        return Product().fromJson(json.data());
      });
      return products.toList();
    });
  }

  Stream<List<Product>> getProductStream() {
    final CollectionReference collectionReference =
        _instanceStore.collection("store");
    return collectionReference
        .orderBy('timestamp', descending: true)
        .snapshots()
        .map(
          (snap) => snap.docs.map((e) => Product().fromJson(e.data())).toList(),
        );
  }
}
