import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/models/SigninUser.dart';
import 'package:shoptime/models/User.dart';
import 'package:shoptime/provider/codeUser.dart';

class Firebaseauth extends ChangeNotifier {
  /*final SigninUser user;
  Firebaseauth({this.user});*/

  bool userConnected = false;
  String errorAuth = '';
  String errorRegister = "pass";
  bool get getStateUser => userConnected;

  String get getErrorAuth => errorAuth;

  updateErrorAuth() {
    errorAuth = '';
  }

  userState() {
    FirebaseAuth auth = FirebaseAuth.instance;

    auth.authStateChanges().listen((event) {
      if (event == null) {
        userConnected = false;
        print("Utilisateur déconecté");
      } else {
        userConnected = true;
        print("Utilisateur connecté");
      }
    });
    notifyListeners();
  }

  Future<UserCredential> registerByMailAndPassword(
      {String mail, String password}) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    return auth.createUserWithEmailAndPassword(email: mail, password: password);
  }

  Future<UserCredential> authSignIn({SigninUser user}) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    return auth.signInWithEmailAndPassword(
      email: user.connectionId,
      password: user.password,
    );
  }

  Future<void> authVerifyPhoneNumber(BuildContext context) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    return auth.verifyPhoneNumber(
      phoneNumber: '+229 67558797',
      timeout: Duration(seconds: 60),
      verificationCompleted: (credential) async {
        UserCredential result = await auth.signInWithCredential(credential);

        User authUser = result.user;

        if (authUser != null) {
          print("Welcome");
        }
      },
      verificationFailed: (e) {
        if (e.code == 'invalid-phone-number') {
          print("Téléphone invalide");
        }
      },
      codeSent: (String verificationId, int resendToken) async {
        String smsCode = Provider.of<CodeUser>(context, listen: false).getCode;

        if (smsCode == "validé") {
          print("Validé");
        }
      },
      codeAutoRetrievalTimeout: (verificationId) {},
    );
  }

  sendVerification() async {
    User user = FirebaseAuth.instance.currentUser;

    if (!user.emailVerified) {
      await user.sendEmailVerification();
    }
    notifyListeners();
  }
  /*RegisterWithEmail() async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: "imdadadelabou0@gmail.com", password: "avatarimdad96");
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print("The password is too weak");
      } else if (e.code == 'email-already-in-use') {
        print("The account already exists for that email.");
      }
    } catch (e) {
      print(e.toString());
    }
  }*/

  /*Future<UserCredential> signInWithGoogle() async {
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }*/

  Future<void> authSignOut() {
    return FirebaseAuth.instance.signOut();
  }
}
