import 'package:shared_preferences/shared_preferences.dart';

class SharePreferencesProduct {
  Future<bool> setIdList({String id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    List<String> oldList = prefs.getStringList('productIds') ?? [];
    oldList.add(id);
    return prefs.setStringList('productIds', oldList);
  }

  Future<bool> setOrderId({String id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    List<String> oldList = prefs.getStringList('orderIds') ?? [];
    oldList.add(id);
    return prefs.setStringList('orderIds', oldList);
  }

  Future<List<String>> getOrderId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getStringList('orderIds') ?? [];
  }

  Future<List<String>> getIdList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList('productIds') ?? [];
  }

  Future<bool> setListId(List<String> list) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setStringList('productIds', list);
  }
}
