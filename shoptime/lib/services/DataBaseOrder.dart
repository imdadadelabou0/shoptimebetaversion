import 'package:flutter/foundation.dart';
import 'package:shoptime/models/orderProduct.dart';
import 'package:shoptime/models/product.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DataBaseOrder {
  Database _dbOrder;

  Future create() async {
    var path = await getDatabasesPath();
    String dbOrderPath = join(path, "dataBaseOrder.db");
    print(dbOrderPath);
    _dbOrder =
        await openDatabase(dbOrderPath, version: 1, onCreate: this._onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute("""CREATE TABLE orderProducts (
      id INTEGER PRIMARY KEY,
      name TEXT NOT NULL,
      description TEXT NOT NULL,
      price TEXT NOT NULL,
      category TEXT NOT NULL,
      productImage TEXT NOT NULL,
      timeStamp TEXT NOT NULL,
      fireId TEXT NOT NULL
    )""");
  }

  Future<OrderProduct> insertOrderProduct(OrderProduct orderproduct) async {
    orderproduct.id =
        await _dbOrder.insert("orderProducts", orderproduct.toMap());
    return orderproduct;
  }

  Future<void> updateOrderProduct(OrderProduct orderProduct) async {
    await _dbOrder.update("orderProducts", orderProduct.toMap(),
        where: "id = ?", whereArgs: [orderProduct.id]);
  }

  Future<OrderProduct> getOrderProduct(int id) async {
    List<Map> maps = await _dbOrder.query("orderProducts",
        columns: OrderProduct.columns, where: "id = ?", whereArgs: [id]);
    OrderProduct orderProduct = OrderProduct.fromMap(maps.first);
    return orderProduct;
  }

  Future<List<OrderProduct>> recentOrder() async {
    var limit = Sqflite.firstIntValue(
        await _dbOrder.rawQuery('SELECT COUNT(*) FROM orderProducts'));
    List<Map> results = await _dbOrder.query("orderProducts",
        columns: OrderProduct.columns, limit: limit, orderBy: "id DESC");
    List<OrderProduct> orderList = new List();
    results.forEach((element) {
      OrderProduct order = OrderProduct.fromMap(element);
      orderList.add(order);
    });
    return orderList;
  }

  deleteOrder(int id) async {
    await _dbOrder.delete("orderProducts", where: "id = ?", whereArgs: [id]);
  }

  nbLigne() async {
    var limit = Sqflite.firstIntValue(
        await _dbOrder.rawQuery('SELECT COUNT(*) FROM orderProducts'));
    return limit;
  }
}
