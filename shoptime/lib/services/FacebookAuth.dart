import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;

class CustomFacebookAuth extends ChangeNotifier {
  Map<String, dynamic> userProfile;
  UserCredential _userCredential;
  String messageConnection = " ";
  var facebookLogin = FacebookLogin();

  facebookSiginWith(FacebookLoginResult facebookLoginResult) async {
    final FacebookAuthCredential facebookAuthCredential =
        FacebookAuthProvider.credential(facebookLoginResult.accessToken.token);
    var userCredential = await FirebaseAuth.instance
        .signInWithCredential(facebookAuthCredential);
    _userCredential = userCredential;
    print(_userCredential);
    notifyListeners();
  }

  Future<Null> initiateFacebookLogin() async {
    var facebookLoginResult = await facebookLogin.logIn(['email']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        messageConnection = "error";
        break;
      case FacebookLoginStatus.cancelledByUser:
        messageConnection = "Arreter par l'utilisateur";
        break;
      case FacebookLoginStatus.loggedIn:
        messageConnection = "Connecté";
        facebookSiginWith(facebookLoginResult);
        var response = await http.get(
            'https://graph.facebook.com/${facebookLoginResult.accessToken.userId}?fields=id,picture,name,email,birthday,hometown&access_token=${facebookLoginResult.accessToken.token}');
        var profile = jsonDecode(response.body);
        print(profile.toString());
        userProfile = profile;
        break;
    }
    notifyListeners();
  }

  Future<Null> logOut() async {
    await facebookLogin.logOut();
    userProfile = null;
    _userCredential = null;
    print("logout");
  }
}
