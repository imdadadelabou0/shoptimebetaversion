import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/provider/codeUser.dart';

class Confirmation extends StatefulWidget {
  _Confirmation createState() => _Confirmation();
}

class _Confirmation extends State<Confirmation> {
  Widget build(context) {
    double width = MediaQuery.of(context).size.width * .9;
    double height = MediaQuery.of(context).size.height * .1;
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: MediaQuery.of(context).size.height / 2 * .2),
            Align(
              alignment: Alignment.center,
              child: Text(
                "ENTER CONFIRMATION CODE",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 4 * .1),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Enter the 6-digit code we sent to +229 67558797",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: Colors.grey),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 4 * .1,
            ),
            Text(
              "Request a new one.",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Colors.blue),
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 6 * .2),
            ConstrainedBox(
              constraints: BoxConstraints.tight(Size(width, height)),
              child: Theme(
                data: ThemeData(
                  primaryColor: Colors.grey,
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Confirmation Code",
                  ),
                  cursorColor: Colors.grey,
                  onChanged: (String code) {
                    setState(() {
                      Provider.of<CodeUser>(context).updateCode(code);
                    });
                  },
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width / 2 * 1.8,
                height: ((MediaQuery.of(context).size.height / 4) + 30) * .2,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Text(
                    "Next",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    //Navigator.pushNamed(context, "/Confirmation");
                  },
                )),
          ],
        ),
      ),
    );
  }
}
