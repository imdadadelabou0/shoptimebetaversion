import 'package:kkiapay_flutter_sdk/kkiapayWebview.dart';

class MyKKiapay {
  KKiaPay initKKiapay(
      {Function callback,
      int amount,
      bool sandbox,
      String data,
      String apiKey,
      String phone,
      String theme}) {
    KKiaPay kkiaPay = KKiaPay(
        callback: callback,
        amount: amount,
        sandbox: sandbox,
        data: data,
        apikey: apiKey,
        phone: phone,
        theme: theme);
    return kkiaPay;
  }
}
