import 'package:flutter/material.dart';

class Count extends StatefulWidget {
  _Count createState() => _Count();
}

class _Count extends State<Count> {
  int add = 1;
  int remove = 0;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            setState(() {
              add = add + 1;
            });
          },
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(),
            ),
            child: IconButton(
              icon: Icon(Icons.add),
              onPressed: null,
            ),
          ),
        ),
        SizedBox(width: 15.0),
        Text("$add"),
        SizedBox(width: 15.0),
        GestureDetector(
          onTap: () {
            setState(() {
              if (add >= 1) {
                add = add - 1;
              }
            });
          },
          child: Container(
            padding: const EdgeInsets.all(3.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(),
            ),
            child: Icon(
              Icons.remove,
            ),
          ),
        ),
      ],
    );
  }
}
