import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/models/orderProduct.dart';
import 'package:shoptime/provider/Verified.dart';
import 'package:shoptime/provider/cart_model.dart';
import 'package:shoptime/provider/listOrder.dart';
import 'package:shoptime/screens/screen3/OderScreen.dart';
import 'package:shoptime/services/DataBaseOrder.dart';
import 'package:sqflite/sqflite.dart';
import 'count.dart';
import 'package:shoptime/models/product.dart';

class DetailScreen extends StatefulWidget {
  final Product myProduct;

  DetailScreen({this.myProduct});

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  bool isLoading = false;
  bool verified = false;

  void _addToCart(BuildContext context, DataBaseOrder dbOrder) async {
    Provider.of<CartModel>(context, listen: false).add(widget.myProduct.id);
    OrderProduct orderProduct =
        OrderProduct().copyToOrder(product: widget.myProduct);
    await dbOrder.insertOrderProduct(orderProduct);
    var nb = await dbOrder.nbLigne();
    print(nb);
    Provider.of<ListOrder>(context, listen: false)
        .addId((orderProduct.id).toString());
    Provider.of<ListOrder>(context, listen: false).update(orderProduct.id);
  }

  // @override
  Widget progress() {
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      backgroundColor: Colors.green,
    );
    //Icon(Icons.verified_rounded),
  }

  void checkIfVerified(BuildContext context) {
    var listId = Provider.of<CartModel>(context, listen: false).productIds;

    for (int i = 0; i < listId.length; i++) {
      if (widget.myProduct.id == listId[i]) {
        verified = true;
        break;
      } else {
        verified = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    checkIfVerified(context);
    var dbOrder = Provider.of<ListOrder>(context, listen: true).getInstance;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            Expanded(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.chevron_left),
                          onPressed: () => Navigator.pop(context),
                        ),
                        Padding(
                          padding: EdgeInsets.all(3.0),
                          child: Stack(
                            children: [
                              IconButton(
                                icon: Icon(Icons.shopping_basket),
                                onPressed: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => OderScreen(),
                                    )),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                  padding: const EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.blue),
                                    color: Colors.blue,
                                  ),
                                  child: Consumer<CartModel>(
                                    builder: (context, cartModel, _) {
                                      int totalInCart =
                                          cartModel.productIds.length;
                                      return Text(
                                        "$totalInCart",
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned.fill(
                    child: Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      child: Hero(
                        tag: widget.myProduct.productImage,
                        child: CachedNetworkImage(
                          width: MediaQuery.of(context).size.width * .7,
                          imageUrl: widget.myProduct.productImage,
                          progressIndicatorBuilder:
                              (context, url, downloadProgress) =>
                                  CircularProgressIndicator(
                            value: downloadProgress.progress,
                          ),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.blueGrey,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                  ),
                ),
                padding: EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          widget.myProduct.name,
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.favorite_border,
                            color: Colors.red,
                          ),
                          onPressed: () {},
                        )
                      ],
                    ),
                    SizedBox(height: 15.0),
                    Text(
                      "Description",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Text(
                      widget.myProduct.description,
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Count(),
                        Text(
                          widget.myProduct.price,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ],
                    ),
                    Spacer(),
                    Container(
                      width: double.infinity,
                      child: RaisedButton(
                        color: Colors.green,
                        child: isLoading
                            ? progress()
                            : verified
                                ? Icon(Icons.verified)
                                : Text(
                                    "Add to Cart",
                                    style: Theme.of(context)
                                        .textTheme
                                        .button
                                        .apply(
                                          color: Colors.white,
                                        ),
                                  ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        onPressed: () {
                          if (verified) {
                            return;
                          }
                          setState(() {
                            isLoading = true;
                          });
                          _addToCart(context, dbOrder);
                          Timer(Duration(seconds: 2), () {
                            setState(() {
                              verified = true;
                              isLoading = false;
                            });
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
