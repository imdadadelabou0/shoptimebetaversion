import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:shoptime/models/product.dart';
import 'package:shoptime/services/StoreBase.dart';
import 'package:shoptime/services/cloud_storage_services.dart';
import 'MyCustomName.dart';
import 'MyCustomDescription.dart';
import 'MyCustomPrice.dart';
import 'dart:io';

class TakeInformation extends StatefulWidget {
  _TakeInformation createState() => _TakeInformation();
}

class _TakeInformation extends State<TakeInformation> {
  ImagePicker _picker = ImagePicker();
  bool isAdd = true;
  CloudStorageResult aboutUrl;
  File takeImg;
  int selectedIndex;
  String name = '';
  String description = '';
  String price = '';
  String category = '';

  final StoreBase myStore = StoreBase();

  Future<void> loadPicker(ImageSource source, BuildContext context) async {
    final pickedImg = await _picker.getImage(source: source);
    if (pickedImg != null) {
      setState(() {
        takeImg = File(pickedImg.path);
      });
      Navigator.pop(context);
    }
  }

  showPickOptionsDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              onTap: () {
                loadPicker(ImageSource.gallery, context);
              },
              leading: Icon(MaterialCommunityIcons.image_album),
              title: Text(
                "Choisir dans la gallerie",
              ),
            ),
            ListTile(
              onTap: () {
                loadPicker(ImageSource.camera, context);
              },
              leading: Icon(MaterialCommunityIcons.camera),
              title: Text(
                "Prendre une photo",
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget chooseCat() {
    List<Widget> chip = List();
    List<String> nameCat = [
      "Clothes",
      "Jean and Short",
      "Accessories",
      "Shoes"
    ];
    FilterChip catChip;

    for (int i = 0; i < nameCat.length; i++) {
      catChip = FilterChip(
        label: Text(nameCat[i]),
        selected: selectedIndex == i,
        selectedColor: Colors.blue,
        onSelected: (isSelected) {
          setState(() {
            if (isSelected) {
              selectedIndex = i;
              category = nameCat[selectedIndex];
            }
          });
        },
      );
      chip.add(Padding(
        padding: EdgeInsets.all(10.0),
        child: catChip,
      ));
    }
    return Wrap(
      children: chip,
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Add a Product"),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(children: [
              Positioned(
                child: Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                    color: takeImg != null
                        ? Colors.transparent
                        : Colors.transparent,
                    border: Border(
                      bottom: BorderSide(color: Colors.transparent),
                      top: BorderSide(color: Colors.transparent),
                      left: BorderSide(color: Colors.transparent),
                      right: BorderSide(color: Colors.transparent),
                    ),
                  ),
                  child: takeImg != null
                      ? Image.file(
                          takeImg,
                          //takeImg,
                          fit: BoxFit.contain,
                        )
                      : Text("No picture"),
                ),
              ),
              Positioned(
                bottom: -11,
                right: -10,
                child: IconButton(
                  onPressed: () {
                    showPickOptionsDialog(context);
                  },
                  icon: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(),
                      color: Colors.blue,
                    ),
                    child: Icon(Icons.add),
                  ),
                ),
              ),
            ]),
            SizedBox(height: 15.0),
            MyCustomName(
              onTextChanged: (String newText) {
                setState(() {
                  name = newText;
                });
              },
            ),
            SizedBox(height: 15.0),
            MyCustomDescription(
              onDescription: (String newDescription) {
                setState(() {
                  description = newDescription;
                });
              },
            ),
            SizedBox(height: 15.0),
            MyCustomPrice(
              onPrice: (String newPrice) {
                setState(() {
                  price = newPrice;
                });
              },
            ),
            SizedBox(height: 9.0),
            Text("Category"),
            Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                chooseCat(),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.all(8.5),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border(
            top: BorderSide(width: 1.0, color: Colors.blue),
            left: BorderSide(width: 1.0, color: Colors.blue),
            right: BorderSide(width: 1.0, color: Colors.blue),
            bottom: BorderSide(width: 1.0, color: Colors.blue),
          ),
          color: Colors.blue,
        ),
        child: FlatButton(
          onPressed: () async {
            if (name.isNotEmpty &&
                description.isNotEmpty &&
                price.isNotEmpty &&
                category.isNotEmpty &&
                takeImg != null) {
              setState(() {
                isAdd = false;
              });
              CloudStorageResult aboutUrl = await CloudStorageService()
                  .uploadImg(imageProduct: takeImg, id: "1");
              final Product myProduct = Product(
                name: name,
                description: description,
                price: price,
                productImage: aboutUrl.imgUrlProduct,
                category: category,
              );
              myStore.addProduct(myProduct).then((_) => Navigator.pop(context));
            }
          },
          child: isAdd == true
              ? Text(
                  "Add to store",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )
              : CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                  backgroundColor: Colors.white,
                ),
        ),
      ),
    );
  }
}
