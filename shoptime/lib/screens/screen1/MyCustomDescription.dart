import 'package:flutter/material.dart';

class MyCustomDescription extends StatefulWidget {
  final Function(String) onDescription;

  const MyCustomDescription({Key key, this.onDescription}) : super(key: key);

  _MyCustomDescription createState() => _MyCustomDescription();
}

class _MyCustomDescription extends State<MyCustomDescription> {
  final myController = TextEditingController();

  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(context) {
    return TextField(
      controller: myController,
      maxLines: 5,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.send,
      //autocorrect: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Description",
      ),
      onChanged: widget.onDescription,
    );
  }
}
