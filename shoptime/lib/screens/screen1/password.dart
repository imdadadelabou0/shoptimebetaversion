import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class LengthPassword extends TextFieldValidator {
  LengthPassword(String errorText) : super(errorText);

  @override
  bool get ignoreEmptyValues => true;

  bool isValid(String value) {
    print("ok");
    // OKay voici ton erreur
    // return '
    return value.length < 6;
  }
}

class Password extends StatefulWidget {
  _Password createState() => _Password();
}

class _Password extends State<Password> {
  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: "The field is required"),
    LengthPassword("ok"),
  ]);
  Widget build(context) {
    return Theme(
      data: ThemeData(primaryColor: Colors.grey),
      child: TextFormField(
        cursorColor: Colors.grey,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.lock),
          hintText: "Type your password",
          border: UnderlineInputBorder(),
        ),
        validator: passwordValidator,
      ),
    );
  }
}
