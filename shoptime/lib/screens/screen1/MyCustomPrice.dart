import 'package:flutter/material.dart';

class MyCustomPrice extends StatefulWidget {
  final Function(String price) onPrice;

  const MyCustomPrice({Key key, this.onPrice}) : super(key: key);
  _MyCustomPrice createState() => _MyCustomPrice();
}

class _MyCustomPrice extends State<MyCustomPrice> {
  final myController = TextEditingController();

  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(context) {
    return Container(
      width: 100,
      child: TextField(
        controller: myController,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.send,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Price",
        ),
        onChanged: widget.onPrice,
      ),
    );
  }
}
