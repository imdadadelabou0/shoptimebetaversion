import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class PhoneField extends StatefulWidget {
  final Function(String connectionId) onConnectionId;
  PhoneField({this.onConnectionId});
  _PhoneField createState() => _PhoneField();
}

class _PhoneField extends State<PhoneField> {
  final _controller = TextEditingController();

  void iniState() {
    super.initState();
  }

  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .9,
      //height: 50,
      child: TextFormField(
        controller: _controller,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Colors.grey[200]),
          ),
          disabledBorder:
              OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          hintText: "Phone-number, username, e-mail",
          fillColor: Colors.grey[200],
          filled: true,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        ),
        onChanged: widget.onConnectionId,
        validator: RequiredValidator(errorText: "The field is required"),
      ),
    );
  }
}
