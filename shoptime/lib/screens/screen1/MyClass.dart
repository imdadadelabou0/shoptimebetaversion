import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class Category {
  String title;
  IconData icon;

  Category({this.title, this.icon});
}

List<Category> cats = [
  Category(title: "Product", icon: MaterialIcons.home),
  Category(title: "Clothes", icon: MaterialCommunityIcons.hanger),
  Category(title: "Jean and Short", icon: MaterialCommunityIcons.alpha_j),
  Category(title: "Accessories", icon: MaterialCommunityIcons.glasses),
  Category(title: "Shoes", icon: MaterialCommunityIcons.shoe_print),
];
