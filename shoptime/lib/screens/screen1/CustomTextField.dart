import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final String label;
  final Function(String text) updateText;
  CustomTextField({this.label, this.updateText});
  _CustomTextField createState() => _CustomTextField();
}

class _CustomTextField extends State<CustomTextField> {
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width * .9;
    var height = MediaQuery.of(context).size.height / 4 * .4;
    return ConstrainedBox(
      constraints: BoxConstraints.tight(Size(width, height /*350, 75*/)),
      child: Theme(
        data: ThemeData(
          primaryColor: Colors.grey,
        ),
        child: TextFormField(
          keyboardType: TextInputType.emailAddress,
          enableSuggestions: true,
          decoration: InputDecoration(
            enabledBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
            disabledBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
            border: OutlineInputBorder(),
            hintText: widget.label,
          ),
          validator: (String text) {
            if (text.isEmpty) {
              return 'Please enter some text';
            }
            if (widget.label == "Password") {
              if (text.length < 6) {
                return 'The password must contain at least 6 characters';
              }
            }
            return null;
          },
          onChanged: widget.updateText,
        ),
      ),
    );
  }
}
