import 'package:flutter/material.dart';
import 'package:shoptime/models/product.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shoptime/screens/screen2/DetailScreen.dart';

class InfoProduct extends StatefulWidget {
  final Product myProduct;
  const InfoProduct({this.myProduct});

  @override
  _InfoProductState createState() => _InfoProductState();
}

class _InfoProductState extends State<InfoProduct> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailScreen(myProduct: widget.myProduct)),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Text(
                widget.myProduct.price,
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: Colors.black,
                    ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: Hero(
                  tag: widget.myProduct.productImage,
                  child: CachedNetworkImage(
                    imageUrl: widget.myProduct.productImage,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(15.0),
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(9.0),
                color: Colors.black,
              ),
              child: Text(
                widget.myProduct.name,
                style: Theme.of(context).textTheme.subtitle2.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
