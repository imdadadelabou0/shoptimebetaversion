import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/models/SigninUser.dart';
import 'package:shoptime/screens/screen1/password.dart';
import 'package:shoptime/screens/screen1/username.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shoptime/services/firebase_auth.dart';

class MyLogin extends StatefulWidget {
  _MyLogin createState() => _MyLogin();
}

class _MyLogin extends State<MyLogin> {
  final _formKey = GlobalKey<FormState>();
  String connectionId = '';
  String password = '';

  Future<Null> validatorSignIn(context, errorCode, username) async {
    if (errorCode == "user-not-found") {
      return _showDialog(
          context, "Username $username not exist.", "", errorCode);
    } else if (errorCode == "wrong-password") {
      return _showDialog(
        context,
        "Incorrect password",
        "The password you entered is incorrect. Please try again.",
        errorCode,
      );
    }
  }

  Future<Null> _showDialog(
    BuildContext context,
    String errorText,
    String user,
    String errorAuth,
  ) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  errorText,
                  style: errorAuth == "wrong-password"
                      ? Theme.of(context).textTheme.headline6
                      : Theme.of(context).textTheme.bodyText1,
                ),
                SizedBox(height: 5.0),
                Text(
                  user,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .apply(color: Colors.grey),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Divider(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Text(
                      "Retry",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget build(context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              "assets/imgs/jospin1.jpeg",
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: MediaQuery.of(context).size.height / 4 * .6),
            Text(
              "Shoptime",
              style: GoogleFonts.sacramento(
                fontStyle: FontStyle.italic,
                fontSize: 50,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 2 * .2),
            Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.05),
              child: Text(
                "Username",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05),
              child: ConstrainedBox(
                constraints: BoxConstraints.tight(Size(340, 80)),
                child: Username(),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.05),
              child: Text(
                "Password",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.05,
              ),
              child: ConstrainedBox(
                constraints: BoxConstraints.tight(Size(340, 60)),
                child: Password(),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * 0.12,
              ),
              child: Align(
                alignment: Alignment.topRight,
                child: Text(
                  "Forgot Password? ",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            SizedBox(height: 30.0),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 45,
                width: 300,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100.0),
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Colors.blue,
                      Colors.purple,
                    ],
                  ),
                ),
                child: Center(
                  child: FlatButton(
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        await Provider.of<Firebaseauth>(context, listen: false)
                            .authSignIn(
                          user: SigninUser(
                            connectionId: connectionId,
                            password: password,
                          ),
                        )
                            .then((value) {
                          print(value.user.email);
                          Navigator.pushNamed(context, "/EssPage");
                        }).catchError(
                          (e) => validatorSignIn(context, e.code, e.email),
                        );
                      }
                    },
                    child: Text(
                      "LOGIN",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Or Sign Up Using",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            SizedBox(height: 19.0),
            Center(
              child: RawMaterialButton(
                onPressed: () {},
                shape: CircleBorder(),
                fillColor: Colors.blue,
                padding: EdgeInsets.all(15.0),
                elevation: 2.0,
                child: FaIcon(
                  FontAwesomeIcons.facebookF,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ), /*Form(
        key: _formKey,
        child: Stack(
          //mainAxisAlignment: MainAxisAlignment.center,
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    "assets/imgs/jospin1.jpeg",
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 4 * .6),
            Text(
              "Shoptime",
              style: GoogleFonts.sacramento(
                fontStyle: FontStyle.italic,
                fontSize: 50,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 2 * .2),
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.05),
                    child: Text(
                      "Username",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width * 0.05),
                    child: ConstrainedBox(
                      constraints: BoxConstraints.tight(Size(340, 80)),
                      child: Username(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.05),
                    child: Text(
                      "Password",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.05,
                    ),
                    child: ConstrainedBox(
                      constraints: BoxConstraints.tight(Size(340, 60)),
                      child: Password(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      right: MediaQuery.of(context).size.width * 0.12,
                    ),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        "Forgot Password? ",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ),
                  SizedBox(height: 30.0),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 45,
                      width: 300,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100.0),
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            Colors.blue,
                            Colors.purple,
                          ],
                        ),
                      ),
                      child: Center(
                        child: FlatButton(
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              await Provider.of<Firebaseauth>(context,
                                      listen: false)
                                  .authSignIn(
                                user: SigninUser(
                                  connectionId: connectionId,
                                  password: password,
                                ),
                              )
                                  .then((value) {
                                print(value.user.email);
                                Navigator.pushNamed(context, "/EssPage");
                              }).catchError(
                                (e) =>
                                    validatorSignIn(context, e.code, e.email),
                              );
                            }
                          },
                          child: Text(
                            "LOGIN",
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Or Sign Up Using",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  SizedBox(height: 19.0),
                  Center(
                    child: RawMaterialButton(
                      onPressed: () {},
                      shape: CircleBorder(),
                      fillColor: Colors.blue,
                      padding: EdgeInsets.all(15.0),
                      elevation: 2.0,
                      child: FaIcon(
                        FontAwesomeIcons.facebookF,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),*/
      /*bottomNavigationBar: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Divider(),
          Padding(
            padding: EdgeInsets.only(top: 3.0, bottom: 8.0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed("/SignUp");
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Don't have an account?",
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(color: Colors.grey)),
                    Text(
                      " Sign up.",
                      style: Theme.of(context).textTheme.bodyText2.copyWith(
                            color: Colors.blueAccent,
                          ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),*/
    );
  }
}
