import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/models/SigninUser.dart';
import 'package:shoptime/screens/screen1/PhoneField.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shoptime/screens/screen1/passwordField.dart';
import 'package:shoptime/services/firebase_auth.dart';
import 'package:shoptime/screens/screen1/showDialogue.dart';

class LoginPage extends StatefulWidget {
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  String connectionId = '';
  String password = '';

  Future<Null> validatorSignIn(context, errorCode, username) async {
    if (errorCode == "user-not-found") {
      return _showDialog(
          context, "Username $username not exist.", "", errorCode);
    } else if (errorCode == "wrong-password") {
      return _showDialog(
        context,
        "Incorrect password",
        "The password you entered is incorrect. Please try again.",
        errorCode,
      );
    }
    //Provider.of<Firebaseauth>(context, listen: false).updateErrorAuth();
  }

  Future<Null> _showDialog(
    BuildContext context,
    String errorText,
    String user,
    String errorAuth,
  ) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            /*shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),*/
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  errorText,
                  style: errorAuth == "wrong-password"
                      ? Theme.of(context).textTheme.headline6
                      : Theme.of(context).textTheme.bodyText1,
                ),
                SizedBox(height: 5.0),
                Text(
                  user,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .apply(color: Colors.grey),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Divider(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Text(
                      "Retry",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff9f9f9),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Column(
                  children: [
                    SizedBox(height: MediaQuery.of(context).size.height * .2),
                    Text(
                      "Shoptime",
                      style: GoogleFonts.pacifico(
                        fontStyle: FontStyle.italic,
                        fontSize: 50,
                      ),
                    ),
                    SizedBox(height: 30),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.05),
                      child: PhoneField(onConnectionId: (String _connectionId) {
                        setState(() {
                          connectionId = _connectionId;
                        });
                      }),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.05),
                      child: PasswordField(onPassword: (String newPassword) {
                        setState(() {
                          password = newPassword;
                        });
                      }),
                    ),
                    SizedBox(height: 20.0),
                    RaisedButton(
                      child: Container(
                        child: Text("Log In"),
                      ),
                      disabledColor: Colors.blue,
                      disabledTextColor: Colors.white,
                      textColor: Colors.white,
                      color: Colors.blue,
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          await Provider.of<Firebaseauth>(context,
                                  listen: false)
                              .authSignIn(
                            user: SigninUser(
                              connectionId: connectionId,
                              password: password,
                            ),
                          )
                              .then((value) {
                            print(value.user.email);
                            Navigator.pushNamed(context, "/EssPage");
                          }).catchError(
                            (e) => validatorSignIn(context, e.code, e.email),
                          );
                        }
                      },
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Divider(),
                      Padding(
                        padding: EdgeInsets.only(top: 3.0, bottom: 8.0),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pushNamed("/SignUp");
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Don't have an account?",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .copyWith(color: Colors.grey)),
                                Text(
                                  " Sign up.",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .copyWith(
                                        color: Colors.blueAccent,
                                      ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
