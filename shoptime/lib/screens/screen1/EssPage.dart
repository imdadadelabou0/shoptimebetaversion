import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class EssPage extends StatefulWidget {
  _EssPage createState() => _EssPage();
}

class _EssPage extends State<EssPage> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          child: Text("sign out"),
          onPressed: () {
            FirebaseAuth.instance.signOut();
            Navigator.of(context).pop();
          },
        ),
      ),
    );
  }
}
