import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class Username extends StatefulWidget {
  _Username createState() => _Username();
}

class _Username extends State<Username> {
  Widget build(context) {
    return Theme(
      data: ThemeData(
        primaryColor: Colors.grey,
      ),
      child: TextFormField(
        cursorColor: Colors.grey,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.person),
          hintText: "Phone-number, username, e-mail",
          border: UnderlineInputBorder(),
        ),
        validator: RequiredValidator(errorText: "The field is required"),
      ),
    );
  }
}
