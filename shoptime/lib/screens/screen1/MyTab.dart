import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/provider/ProviderEmail.dart';
import 'package:shoptime/screens/screen1/RegisterEmail.dart';
import 'package:shoptime/screens/screen1/TelSignUp.dart';

class MyTab extends StatefulWidget {
  _MyTab createState() => _MyTab();
}

class _MyTab extends State<MyTab> with SingleTickerProviderStateMixin {
  void iniState() {
    super.initState();
  }

  Widget build(context) {
    String mail = '';
    return DefaultTabController(
      length: 2,
      child: Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TabBar(
              indicatorColor: Colors.black,
              indicatorPadding: EdgeInsets.symmetric(horizontal: 20.0),
              tabs: [
                Tab(
                  child: Text(
                    "TÉLÉPHONE",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                Tab(
                  child: Text(
                    "E-MAIL",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  SingleChildScrollView(child: TelSignUp()),
                  SingleChildScrollView(child: RegisterEmail(
                    updateMail: (String newMail) {
                      setState(() {
                        mail = newMail;
                      });
                      Provider.of<ProviderEmail>(context, listen: false)
                          .setMail(mail);
                      //print(context.watch<ProviderEmail>().mail);
                    },
                  )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
