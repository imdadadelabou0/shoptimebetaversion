import 'package:flutter/material.dart';
import 'package:path/path.dart';

Future<void> _showDialog(BuildContext context) async {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Mot de passe incorrect pour \n real"),
              SizedBox(
                height: 10.0,
              )
            ],
          ),
          actions: [
            Divider(),
            ListTile(
              title: Text("Try"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      });
}
