import 'package:flutter/material.dart';

class VerifyEmail extends StatefulWidget {
  final String email;
  VerifyEmail({this.email});
  _VerifyEmail createState() => _VerifyEmail();
}

class _VerifyEmail extends State<VerifyEmail> {
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 2 * .1),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "ENTER THE CONFIRMATION CODE",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "Enter the confirmation code that we \nhave sent to \n${widget.email}",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            FlatButton(
                onPressed: () {},
                child: Text(
                  "Resend The Code",
                  style: TextStyle(color: Colors.blue),
                )),
            SizedBox(
              height: MediaQuery.of(context).size.height / 4 * .1,
            ),
            ConstrainedBox(
              constraints: BoxConstraints.tight(Size(350, 50)),
              child: Theme(
                data: ThemeData(
                  primaryColor: Colors.grey,
                ),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  enableSuggestions: true,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    disabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 4 * .1),
            Container(
                width: MediaQuery.of(context).size.width / 2 * 1.7,
                height: ((MediaQuery.of(context).size.height / 4) + 30) * .2,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Text(
                    "Next",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  color: Colors.blue,
                  onPressed: () async {},
                )),
          ],
        ),
      ),
    );
  }
}
