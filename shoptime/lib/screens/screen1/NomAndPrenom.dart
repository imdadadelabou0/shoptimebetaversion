import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shoptime/screens/generalScreen/firstScreen.dart';
import 'package:shoptime/screens/screen1/CustomTextField.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';

class NomAndPrenom extends StatefulWidget {
  _NomAndPrenom createState() => _NomAndPrenom();
}

class _NomAndPrenom extends State<NomAndPrenom> {
  bool showText = true;
  final _formKey = GlobalKey<FormState>();
  //GifController controller= GifController(vsync: );

  Future showDialogue(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
            width: 100,
            height: 300,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Center(
                    child: CupertinoActivityIndicator(
                      radius: 50,
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Please wait.",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .4,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(85)),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height / 4 * .6),
                      Align(
                        alignment: Alignment.center,
                        child: Image.asset("assets/imgs/registerIcon.png"),
                      ),
                      Spacer(),
                      Padding(
                        padding: EdgeInsets.all(30),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: Text(
                            "Register",
                            style: GoogleFonts.roboto(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height / 4 * .3),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "NAME AND PASSWORD",
                      style: GoogleFonts.lato(
                          textStyle: Theme.of(context).textTheme.subtitle1),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 8 * .2,
                ),
                CustomTextField(
                  label: "Name and FirstName",
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 4 * .1),
                CustomTextField(
                  label: "Password",
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 4 * .2),
                Container(
                    width: MediaQuery.of(context).size.width / 2 * 1.7,
                    height:
                        ((MediaQuery.of(context).size.height / 4) + 30) * .2,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      child: showText
                          ? Text(
                              "Next",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )
                          : CircularProgressIndicator(
                              backgroundColor: Colors.blue,
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white)),
                      color: Colors.blue,
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          showDialogue(context);
                          Future.delayed(
                            Duration(seconds: 5),
                            () => Navigator.pushReplacementNamed(
                              context,
                              "/FirstScreen",
                              /*MaterialPageRoute(
                                builder: (context) => FirstScreen(),
                              ),*/
                            ),
                          );
                        }
                        //  Provider.of<ProviderEmail>(context, listen: false).getMail);
                      },
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
