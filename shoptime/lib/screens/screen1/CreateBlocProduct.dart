import 'package:flutter/material.dart';
import 'package:form_bloc/form_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class CreateBlocProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TextFieldBlocBuilder(
          textFieldBloc: TextFieldBloc(),
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: "Name",
            fillColor: Colors.blue,
          ),
        ),
      ],
    );
  }
}
