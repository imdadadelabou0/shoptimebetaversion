import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shoptime/screens/screen1/MyTab.dart';
import 'package:shoptime/screens/screen1/SignUp.dart';

class SignUp extends StatefulWidget {
  _SignUp createState() => _SignUp();
}

class _SignUp extends State<SignUp> {
  Widget build(context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 155,
            ),
            Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(width: 2.0),
              ),
              child: Center(
                child: Image.asset(
                  "assets/imgs/4be2438fc42df50f5ad294c92676c90e.png",
                  width: 100,
                  height: 100,
                ),
              ),
            ),
            SizedBox(height: 10),
            MyTab(),
          ],
        ),
      ),
    );
  }
}
