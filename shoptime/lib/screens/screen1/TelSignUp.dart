import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/provider/codeUser.dart';
import 'package:shoptime/screens/screen1/CodeField.dart';
import 'package:shoptime/services/firebase_auth.dart';

class TelSignUp extends StatefulWidget {
  _TelSignUp createState() => _TelSignUp();
}

class _TelSignUp extends State<TelSignUp> {
  Widget build(context) {
    return Column(
      children: [
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        ConstrainedBox(
          constraints: BoxConstraints.tight(Size(350, 50)),
          child: Theme(
            data: ThemeData(
              primaryColor: Colors.grey,
            ),
            child: TextFormField(
              keyboardType: TextInputType.number,
              enableSuggestions: true,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                border: OutlineInputBorder(),
                prefixText: "+229",
              ),
            ),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              "You may receive SMS updates from Shoptime",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Colors.grey),
            ),
          ),
        ),
        SizedBox(height: 10),
        Container(
            width: MediaQuery.of(context).size.width / 2 * 1.7,
            height: ((MediaQuery.of(context).size.height / 4) + 30) * .2,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              child: Text(
                "Next",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              color: Colors.blue,
              onPressed: () async {
                Firebaseauth();
                await Provider.of<Firebaseauth>(context, listen: false)
                    .authVerifyPhoneNumber(context)
                    .then((value) =>
                        Navigator.pushNamed(context, "/Confirmation"))
                    .catchError((e) => print(e));
                //auth.authVerifyPhoneNumber();
              },
            )),
      ],
    );
  }
}
