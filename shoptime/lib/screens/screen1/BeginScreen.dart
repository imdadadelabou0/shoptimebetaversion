import 'package:flutter/material.dart';
import 'package:shoptime/connexion/ButtonComponent.dart';
import 'package:shoptime/connexion/CustomLogin.dart';
import 'package:shoptime/screens/generalScreen/CreateYourAccount.dart';

class BeginScreen extends StatefulWidget {
  BeginScreen({Key key}) : super(key: key);

  @override
  _BeginScreenState createState() => _BeginScreenState();
}

class _BeginScreenState extends State<BeginScreen> {
  double paddingOfDivider = 0;
  pushToCreatePageLogin(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CreateYourAccount(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    paddingOfDivider = MediaQuery.of(context).size.width / 8 - 20;
    return Scaffold(
      body: Container(
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/imgs/blacky.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height * .1),
              child: Stack(
                children: [
                  Positioned(
                    left: MediaQuery.of(context).size.width * .7,
                    child: FlatButton(
                      onPressed: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => CustomLogin(),
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "s'identifier",
                            style: TextStyle(
                              fontFamily: "Merriweather",
                              color: Colors.white,
                            ),
                          ),
                          Icon(
                            Icons.chevron_right,
                            color: Colors.pink,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 20.0),
                        child: Center(
                          child: Text(
                            "La boutique idéal pour \nvos achats",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Merriweather",
                              fontSize: 28,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            right: paddingOfDivider, left: paddingOfDivider),
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(height: 25),
                      Center(
                        child: ButtonComponent(
                          text: "s'inscrire",
                          couleur: Colors.white,
                          buttonColor: Colors.pink,
                          ifPressed: () => pushToCreatePageLogin(context),
                        ),
                      ),
                      SizedBox(height: 20),
                      Center(
                        child: ButtonComponent(
                          text: "s'inscrire avec facebook",
                          couleur: Colors.white,
                          ifPressed: () {
                            print("s inscrire avec facebook");
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
