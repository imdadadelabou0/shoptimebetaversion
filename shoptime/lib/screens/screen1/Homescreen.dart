import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/provider/Active.dart';
import 'package:shoptime/screens/screen1/Category.dart';
import 'package:shoptime/screens/screen1/MyClass.dart';
import 'package:shoptime/screens/screen1/TakeInformation.dart';
import 'InfoProduct.dart';
import 'package:shoptime/services/StoreBase.dart';
import 'package:shoptime/models/product.dart';
import 'package:shoptime/provider/cart_model.dart';

class NavItem {
  final IconData icon;
  final String title;

  NavItem({this.icon, this.title});
}

class HomeScreen extends StatefulWidget {
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  int _selectedIndex = 0;
  int countPost = 0;
  String category;
  List<Product> products = [];
  bool isAdmin = true;
  // List<String> listProductInCart = [''];

  List<NavItem> myNav = [
    NavItem(
      icon: Icons.home,
      title: 'Home',
    ),
    NavItem(
      icon: Icons.card_giftcard,
      title: 'Gift',
    ),
    NavItem(
      icon: Icons.favorite,
      title: 'Favorite',
    ),
    NavItem(
      icon: Icons.person,
      title: 'Person',
    )
  ];

  void pushPageTakeInformation(BuildContext context) {
    Navigator.push(
      context,
      PageRouteBuilder(
        transitionDuration: Duration(milliseconds: 850),
        pageBuilder: (context, animation, secondAnimation) {
          return TakeInformation();
        },
        transitionsBuilder: (context, animation, secondAnimation, child) {
          return SlideTransition(
            position: animation.drive(
              Tween(
                begin: Offset(1, 0),
                end: Offset(0, 0),
              ).chain(CurveTween(curve: Curves.ease)),
            ),
            child: child,
          );
        },
      ),
    );
  }

  void iniState() {
    super.initState();
  }

  @override
  Widget build(context) {
    var active = Provider.of<Active>(context, listen: false).getActive;
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        backgroundColor: Color(0xfff9f9f9),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        actions: [
          Padding(
            padding: EdgeInsets.all(3.0),
            child: Stack(
              children: [
                IconButton(
                  icon: Icon(Icons.shopping_basket),
                  onPressed: () {},
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blue),
                      color: Colors.blue,
                    ),
                    child: Consumer<CartModel>(
                      builder: (context, cartModel, _) {
                        int totalInCart = cartModel.productIds.length;
                        return Text(
                          "$totalInCart",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: isAdmin
          ? FloatingActionButton(
              onPressed: () {
                pushPageTakeInformation(context);
              },
              child: Icon(
                Icons.add,
              ),
            )
          : null,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Clothes',
                style: TextStyle(
                  fontSize: 28,
                  color: Colors.black,
                  letterSpacing: 2.0,
                ),
              ),
              Text(
                'Delivery',
                style: TextStyle(
                  fontSize: 40.0,
                  color: Colors.grey,
                ),
              ),
              SizedBox(height: 15.0),
              TextField(
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  border: InputBorder.none,
                  prefixIcon: Icon(Icons.search),
                  hintText: "Search",
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 81,
                child: CategoryContainer(),
              ),
              SizedBox(
                height: 15.0,
              ),
              Text(
                cats[context.watch<Active>().getActive].title,
                style: Theme.of(context).textTheme.headline6.apply(
                      fontWeightDelta: 2,
                    ),
              ),
              SizedBox(
                height: 11,
              ),
              StreamBuilder(
                stream: StoreBase().getProductStream(),
                builder: (context, AsyncSnapshot<List<Product>> snapshot) {
                  var products = snapshot.data;
                  if (active >= 1 && active <= 4) {
                    products = products
                        .where(
                            (element) => element.category == cats[active].title)
                        .toList();
                  }
                  return !snapshot.hasData
                      ? Text("Waiting")
                      : GridView.builder(
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 15,
                            mainAxisSpacing: 15,
                            childAspectRatio: .7,
                          ),
                          itemCount: products
                              .length, //context.watch<ProductList>().listProduct.length,
                          itemBuilder: (BuildContext context, int index) {
                            // var products = context.watch<ProductList>().listProduct;
                            return InfoProduct(myProduct: products[index]);
                          },
                        );
                },
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: isAdmin
          ? SizedBox(
              height: 70,
              child: BottomAppBar(
                shape: CircularNotchedRectangle(),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      buildNavItem(myNav[0], _selectedIndex == 0),
                      buildNavItem(myNav[1], _selectedIndex == 1),
                      Expanded(child: Container()),
                      buildNavItem(myNav[2], _selectedIndex == 2),
                      buildNavItem(myNav[3], _selectedIndex == 3),
                    ],
                  ),
                ),
              ),
            )
          : null,
      backgroundColor: Color(0xfff9f9f9),
    );
  }

  Widget buildNavItem(NavItem navItem, bool active) {
    return Expanded(
      child: InkWell(
        onTap: () {
          setState(() {
            _selectedIndex = myNav.indexOf(navItem);
          });
        },
        child: Container(
          child: Column(
            children: [
              Icon(navItem.icon,
                  color: active ? Colors.black : Colors.grey[300]),
              SizedBox(
                height: 8,
              ),
              Text(
                navItem.title,
                style: TextStyle(
                  color: active ? Colors.black : Colors.grey[300],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
