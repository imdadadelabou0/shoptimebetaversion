import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/provider/ProviderEmail.dart';
import 'package:shoptime/provider/codeUser.dart';
import 'package:shoptime/screens/screen1/CodeField.dart';
import 'package:shoptime/screens/screen1/NomAndPrenom.dart';
import 'package:shoptime/screens/screen1/verifyEmail.dart';
import 'package:shoptime/services/firebase_auth.dart';

class RegisterEmail extends StatefulWidget {
  final Function(String mail) updateMail;
  RegisterEmail({this.updateMail});
  _RegisterEmail createState() => _RegisterEmail();
}

class _RegisterEmail extends State<RegisterEmail> {
  bool showText = true;

  Future<UserCredential> registerWithEmail(String eMail) async {
    //try {
    FirebaseAuth auth = FirebaseAuth.instance;
    return auth.createUserWithEmailAndPassword(
        email: eMail, password: "avatarimdad96");
    /*} on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print("The password is too weak");
      } else if (e.code == 'email-already-in-use') {
        print("The account already exists for that email.");
      }
    } catch (e) {
      print(e.toString());
    }*/
  }

  Future<Null> callDialogue(String messageError, BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
            width: 100,
            height: 300,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              content: Text(
                messageError,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(
                    "Retry",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<Null> showError(String ecode, BuildContext context) {
    if (ecode == 'email-already-in-use') {
      return callDialogue(
          "The account already exists for that email.", context);
    }
    return null;
  }

  Widget build(context) {
    return Column(
      children: [
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        ConstrainedBox(
          constraints: BoxConstraints.tight(Size(350, 50)),
          child: Theme(
            data: ThemeData(
              primaryColor: Colors.grey,
            ),
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              enableSuggestions: true,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                border: OutlineInputBorder(),
                labelText: "E-mail",
              ),
              onChanged: widget.updateMail,
            ),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              "You may receive Mail updates from Shoptime",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Colors.grey),
            ),
          ),
        ),
        SizedBox(height: 10),
        Container(
          width: MediaQuery.of(context).size.width / 2 * 1.7,
          height: ((MediaQuery.of(context).size.height / 4) + 30) * .2,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            child: showText
                ? Text(
                    "Next",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  )
                : CircularProgressIndicator(
                    backgroundColor: Colors.blue,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
            color: Colors.blue,
            onPressed: () async {
              if (showText) {
                setState(() {
                  showText = false;
                });
                String eMail =
                    Provider.of<ProviderEmail>(context, listen: false).getMail;
                await registerWithEmail(eMail).then((value) {
                  setState(() {
                    showText = true;
                  });
                  Provider.of<Firebaseauth>(context, listen: false)
                      .sendVerification();
                  print("ok");
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => NomAndPrenom()));
                  //VerifyEmail(email: value.user.email)));
                  print(value.user.email);
                }).catchError((e) {
                  setState(() {
                    showText = true;
                  });
                  showError(e.code, context);
                });
              }
              //print(
              //  Provider.of<ProviderEmail>(context, listen: false).getMail);
            },
          ),
        ),
      ],
    );
  }
}
