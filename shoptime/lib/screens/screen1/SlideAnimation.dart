import 'package:flutter/material.dart';

class MySlideAnimation extends PageRouteBuilder {
  Widget page;
  double dx, dy, ex, ey;
  MySlideAnimation({this.page, this.dx, this.dy, this.ex, this.ey})
      : super(
          pageBuilder: (context, animation, secondAnimation) => page,
          transitionsBuilder: (context, animation, secondAnimation, child) {
            return SlideTransition(
              position: Tween(
                begin: Offset(1.0, 0.0),
                end: Offset(0.0, 0.0),
              ).animate(animation),
              child: SlideTransition(
                position: Tween(
                  begin: Offset(0.0, 0.0),
                  end: Offset(1.0, 0.0),
                ).animate(secondAnimation),
                child: child,
              ),
            );
          },
        );
}
