import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class PasswordField extends StatefulWidget {
  final Function(String password) onPassword;
  PasswordField({this.onPassword});
  _PasswordField createState() => _PasswordField();
}

class _PasswordField extends State<PasswordField> {
  final _controller = TextEditingController();
  bool hider = true;
  void iniState() {
    super.initState();
  }

  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .9,
      child: TextFormField(
        controller: _controller,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Colors.grey[200]),
          ),
          disabledBorder:
              OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          hintText: "Password",
          fillColor: Colors.grey[200],
          filled: true,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                hider = !hider;
              });
            },
            child: Icon(hider ? Icons.visibility_off : Icons.visibility),
          ),
        ),
        validator: RequiredValidator(
            errorText:
                "The field is required"), //(value) => value.length < 6 ? "ok" : null,
        obscureText: hider,
        onChanged: widget.onPassword,
      ),
    );
  }
}
