import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class MyCustomName extends StatefulWidget {
  final Function(String) onTextChanged;

  const MyCustomName({Key key, this.onTextChanged}) : super(key: key);

  _MyCustomNameState createState() => _MyCustomNameState();
}

class _MyCustomNameState extends State<MyCustomName> {
  FocusNode myFocusNode;
  final myController = TextEditingController();
  void iniState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      focusNode: myFocusNode,
      controller: myController,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.send,
      autocorrect: true,
      decoration: InputDecoration(
        labelText: "Name",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
      onChanged: widget.onTextChanged,
    );
  }
}
