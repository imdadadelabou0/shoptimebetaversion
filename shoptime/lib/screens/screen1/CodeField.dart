import 'package:flutter/material.dart';

class CodeField extends StatefulWidget {
  _CodeField createState() => _CodeField();
}

class _CodeField extends State<CodeField> {
  Widget build(context) {
    return Container(
      alignment: Alignment.center,
      width: 60,
      height: 40,
      padding: EdgeInsets.all(1),
      decoration: BoxDecoration(
        color: Colors.grey,
      ),
      child: Text("+229"),
    );
  }
}
