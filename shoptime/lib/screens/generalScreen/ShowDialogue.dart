import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/provider/UserType.dart';

class ShowDialogue extends StatefulWidget {
  ShowDialogue({Key key}) : super(key: key);

  @override
  _ShowDialogueState createState() => _ShowDialogueState();
}

class _ShowDialogueState extends State<ShowDialogue> {
  bool acheteur = false;
  bool vendeur = false;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Inscrivez-vous en Tant que :",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: "Pacifico",
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          ListTile(
            leading: Checkbox(
              onChanged: (value) async {
                setState(() {
                  acheteur = value;
                  if (acheteur == true) {
                    vendeur = false;
                  }
                });
              },
              activeColor: Colors.pink,
              value: acheteur,
            ),
            title: Text(
              "Acheteur",
              style: TextStyle(
                fontFamily: "MerriweatherR",
              ),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          ListTile(
            leading: Checkbox(
              onChanged: (value) async {
                setState(() {
                  vendeur = value;
                  if (vendeur == true) {
                    acheteur = false;
                  }
                });
              },
              activeColor: Colors.pink,
              value: vendeur,
            ),
            title: Text(
              "Vendeur",
              style: TextStyle(
                fontFamily: "MerriweatherR",
              ),
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          RaisedButton(
            onPressed: () {
              context.read<UserType>().updateValue(acheteur, vendeur);
              Navigator.of(context).pop();
            },
            child: Text(
              "Validate",
              style: TextStyle(
                fontFamily: "MerriweatherR",
              ),
            ),
          ),
        ],
      ),
    );
  }
}
