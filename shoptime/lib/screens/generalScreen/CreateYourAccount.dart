import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/GlobalColorSign.dart';
import 'package:shoptime/connexion/ButtonComponent.dart';
import 'package:shoptime/connexion/CheckBox.dart';
import 'package:shoptime/connexion/CircularProgressIndicator.dart';
import 'package:shoptime/connexion/FormFieldComponent.dart';
import 'package:shoptime/connexion/MainText.dart';
import 'package:shoptime/models/User.dart';
import 'package:shoptime/provider/UserType.dart';
import 'package:shoptime/screens/generalScreen/ShowDialogue.dart';
import 'package:shoptime/screens/generalScreen/ViewClientProductDetail.dart';
import 'package:shoptime/screens/generalScreen/firstScreen.dart';
import 'package:shoptime/screens/screen1/BeginScreen.dart';
import 'package:shoptime/screens/screen1/Homescreen.dart';
import 'package:shoptime/screens/screen1/passwordField.dart';
import 'package:shoptime/services/StockUserData.dart';
import 'package:shoptime/services/firebase_auth.dart';
import 'package:validators/validators.dart';

class CreateYourAccount extends StatefulWidget {
  CreateYourAccount({Key key}) : super(key: key);

  @override
  _CreateYourAccountState createState() => _CreateYourAccountState();
}

class _CreateYourAccountState extends State<CreateYourAccount> {
  String username = " ";
  String numTelOrMail = " ";
  String password = " ";
  String userType = " ";
  String idUser = " ";
  bool wait = false;
  final _key = GlobalKey<FormState>();

  String joinShoptime(String valueToJoin) {
    String shoptime = "@shoptime.com";
    String newValue = '$valueToJoin$shoptime';
    return newValue;
  }

  String removeSpace(String value) {
    int i = 0;
    String newValue = "";
    for (i = 0; i < value.length; i++) {
      if (value[i] != ' ') {
        newValue += value[i];
      }
    }
    return newValue;
  }

  ifNumTel(String identifiant) {
    if (isEmail(identifiant)) {
      numTelOrMail = identifiant;
    } else {
      numTelOrMail = joinShoptime(identifiant);
    }
  }

  verifiedUserInput() {
    //numTelOrMail = removeSpace(numTelOrMail);

    ifNumTel(numTelOrMail);
    print(numTelOrMail);
  }

  connectUser(BuildContext context) async {
    final StockUserData instanceStockUser = StockUserData();
    await instanceStockUser.addUserData(
      Users(
        idUser: idUser,
        username: username,
        numTelOrMail: numTelOrMail,
        password: password,
        userType: userType,
      ),
    );
  }

  switchToHome(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => FirstScreen(),
      ),
    );
  }

  snackBarPage(BuildContext context) {
    final snack = SnackBar(
      content: Text("Email déja utilisé"),
    );
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _key,
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 4 - 10,
              width: double.infinity,
              decoration: BoxDecoration(
                color: couleurEx1,
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 50.0),
                  child: MainText(text: "Créer votre \ncompte"),
                ),
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 40,
                      ),
                      FormFieldComponent(
                        labelText: "Nom d'utilisateur",
                        change: (String newValue) {
                          setState(() {
                            username = newValue;
                          });
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      FormFieldComponent(
                        labelText: "Numero de Téléphone ou E-mail",
                        typeKeyboard: TextInputType.emailAddress,
                        change: (String tel) {
                          setState(() {
                            numTelOrMail = tel;
                          });
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Passwordfield(
                        change: (String value) {
                          setState(() {
                            password = value;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      CheckBox(
                        text:
                            "vous acceptez les conditions et la politique de confidentialité",
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      ButtonTheme(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(30.0))),
                        child: Container(
                          width: MediaQuery.of(context).size.width * .7 + 12,
                          height: 50,
                          child: RaisedButton(
                            color: Colors.pink,
                            child: wait == false
                                ? Text(
                                    "s'inscrire",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "MerriweatherR",
                                    ),
                                  )
                                : CircularIndicator(),
                            onPressed: () {
                              if (_key.currentState.validate()) {
                                showDialog(
                                  context: context,
                                  builder: (context) => ShowDialogue(),
                                ).then(
                                  (value) async {
                                    userType = Provider.of<UserType>(context,
                                            listen: false)
                                        .getUserType;
                                    if ((userType == "acheteur" ||
                                        userType == "vendeur")) {
                                      print("Bien recuuu");
                                      setState(() {
                                        wait = true;
                                      });
                                      verifiedUserInput();
                                      await context
                                          .read<Firebaseauth>()
                                          .registerByMailAndPassword(
                                              mail: numTelOrMail,
                                              password: password)
                                          .then((value) async {
                                        idUser = value.user.uid;
                                        connectUser(context);
                                        setState(() {
                                          wait = false;
                                        });
                                        switchToHome(context);
                                      }).catchError((e) {
                                        setState(() {
                                          wait = false;
                                        });
                                        print(e.code.toString());
                                        snackBarPage(context);
                                      });
                                    }
                                  },
                                );
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Separator(),
                      SizedBox(
                        height: 20.0,
                      ),
                      SignUpWithFacebook(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Separator extends StatelessWidget {
  const Separator({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 40.0, right: 40.0),
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 8.0),
              child: Divider(
                color: Colors.pink,
              ),
            ),
          ),
          Text(
            "OU",
            style: TextStyle(
              fontFamily: "MerriweatherR",
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Divider(
                color: Colors.pink,
              ),
            ),
          )
        ],
      ),
    );
  }
}
