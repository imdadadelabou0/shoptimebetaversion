import 'package:flutter/material.dart';
import 'package:shoptime/screens/generalScreen/WidgetHome.dart';
import 'package:shoptime/screens/generalScreen/Notification.dart';
import 'AboutClient.dart';

List<Widget> allPageHome = [
  WidgetHome(),
  Text("Fil d'Actualité"),
  Text("Delivery"),
  NotificationWidget(),
  AboutClient(),
];
