import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shoptime/Component/MyAppBar.dart';
import 'package:shoptime/screens/generalScreen/AboutClient.dart';
import 'package:shoptime/screens/generalScreen/ListOfPage.dart';
import 'package:shoptime/screens/generalScreen/PopularBrand.dart';
import 'package:shoptime/screens/generalScreen/RecommandedForYou.dart';
import 'package:shoptime/screens/generalScreen/SecondScreen.dart';
import 'package:shoptime/screens/generalScreen/VendeurContactList.dart';
import 'package:shoptime/screens/generalScreen/iconBottom.dart';
import 'package:shoptime/screens/generalScreen/voirplus.dart';

class FirstScreen extends StatefulWidget {
  _FirstScreen createState() => _FirstScreen();
}

class _FirstScreen extends State<FirstScreen> {
  int _selectedIndex = 0;
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        index: _selectedIndex,
      ),
      body: allPageHome[_selectedIndex],
      floatingActionButton: _selectedIndex == 3
          ? FloatingActionButton(
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => VendeurContactList(),
                ),
              ),
              child: SvgPicture.asset(
                "assets/imgs/hipchat.svg",
                width: 30,
                height: 30,
              ),
            )
          : null,
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/imgs/icons8-dog-house-50.png",
              scale: 2.0,
            ),
            label: "Accueil",
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/imgs/icons8-reminders-50.png",
              scale: 2.0,
            ),
            label: "Fil d'Actualité",
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/imgs/icons8-scooter-50.png",
              scale: 2.0,
            ),
            label: "Delivery",
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/imgs/icons8-notification-50.png",
              scale: 2.0,
            ),
            label: "Notifications",
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/imgs/icons8-customer-64.png",
              scale: 2.5,
            ),
            label: "Me",
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black,
        onTap: (int index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        showUnselectedLabels: true,
        selectedFontSize: 12,
        selectedLabelStyle: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
