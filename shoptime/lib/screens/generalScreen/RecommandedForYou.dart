import 'package:flutter/material.dart';
import 'package:shoptime/screens/generalScreen/SecondScreen.dart';

class RecommandedForYou extends StatefulWidget {
  RecommandedForYou({Key key}) : super(key: key);

  @override
  _RecommandedForYouState createState() => _RecommandedForYouState();
}

class _RecommandedForYouState extends State<RecommandedForYou> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          GridView.builder(
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 7,
              mainAxisSpacing: 7,
              childAspectRatio: .6,
            ),
            itemCount: 6,
            itemBuilder: (context, index) {
              return SecondScreen();
            },
          ),
        ],
      ),
    );
  }
}
