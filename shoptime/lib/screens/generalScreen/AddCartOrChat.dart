import 'package:flutter/material.dart';

class AddCartOrChart extends StatefulWidget {
  AddCartOrChart({Key key}) : super(key: key);

  @override
  _AddCartOrChartState createState() => _AddCartOrChartState();
}

class _AddCartOrChartState extends State<AddCartOrChart> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: 80,
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              width: 40,
              height: 70,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
                border: Border.all(
                  color: Colors.grey,
                ),
              ),
              child: Icon(
                Icons.chat,
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: FlatButton(
                  onPressed: () {},
                  child: Text(
                    "Négocier le \nprix",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  color: Colors.black,
                  border: Border.all(),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: FlatButton(
                  onPressed: () => print("ok"),
                  child: Text(
                    "Ajouter au\npanier",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
