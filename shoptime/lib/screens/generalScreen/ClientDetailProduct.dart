import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:shoptime/screens/generalScreen/AddCartOrChat.dart';
import 'package:shoptime/screens/generalScreen/CarouselImage.dart';
import 'package:shoptime/screens/generalScreen/aboutVendeur.dart';

class ClientDetailProduct extends StatefulWidget {
  ClientDetailProduct({Key key}) : super(key: key);

  @override
  _ClientDetailProductState createState() => _ClientDetailProductState();
}

class _ClientDetailProductState extends State<ClientDetailProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xfff9f9f9),
          elevation: 0.0,
          leading: Icon(
            Icons.chevron_left,
            color: Colors.black,
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 6.0),
              child: Icon(
                Icons.shopping_basket_sharp,
                color: Colors.black,
              ),
            )
          ],
        ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.grey.shade200,
                        child: CarouselImage(),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 50.0, bottom: 10.0),
                            child: Text(
                              "LUXURY BROWN BAG", //Nom Produit
                              style: TextStyle(
                                letterSpacing: 2.0,
                                fontFamily: "AudreyNormal",
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Text(
                            "\$100", //Prix du Produit
                            style: TextStyle(
                              //fontWeight: FontWeight.bold,
                              color: Colors.pink,
                            ),
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 50.0),
                            child: Text(
                              "The RichText widget displays text that uses multiple different styles. The text to display is described using a tree of TextSpan objects.",
                              style: TextStyle(
                                fontFamily: "Nexa-Bold",
                                fontSize: 15,
                                color: Colors.grey,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ), //Description Du Produit
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              AddCartOrChart(),
            ],
          ),
        ),
      ),
    );
  }
}
