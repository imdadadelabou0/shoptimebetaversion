import 'package:flutter/material.dart';
import 'package:shoptime/screens/generalScreen/PopularBrand.dart';
import 'package:shoptime/screens/generalScreen/RecommandedForYou.dart';
import 'package:shoptime/screens/generalScreen/SecondScreen.dart';
import 'package:shoptime/screens/generalScreen/voirplus.dart';

class WidgetHome extends StatelessWidget {
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text(
                "Marque Populaire",
                style: TextStyle(
                  fontFamily: "MerriweatherR",
                  fontSize: 20.0,
                ),
              ),
            ),
            Container(
              height: 200,
              child: PopularBrand(),
            ),
            SizedBox(
              height: 30.0,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text(
                "Recommandé Pour Vous",
                style: TextStyle(
                  fontFamily: "MerriweatherR",
                  fontSize: 20.0,
                ),
              ),
            ),
            RecommandedForYou(),
            SizedBox(height: 15.0),
            VoirPlus(),
            SizedBox(height: 30.0),
            Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text(
                "Nouveautés",
                style: TextStyle(
                  fontFamily: "MerriweatherR",
                  fontSize: 20.0,
                ),
              ),
            ),
            GridView.builder(
              scrollDirection: Axis.vertical,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 7,
                mainAxisSpacing: 7,
                childAspectRatio: .6,
              ),
              itemCount: 1,
              itemBuilder: (context, index) {
                return SecondScreen(index: index);
              },
            ),
          ],
        ),
      ),
    );
  }
}
