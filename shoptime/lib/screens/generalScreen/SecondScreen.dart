import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shoptime/screens/generalScreen/ClientDetailProduct.dart';
import 'package:shoptime/screens/generalScreen/SwipeUp.dart';
import 'package:shoptime/screens/generalScreen/ViewClientProductDetail.dart';

class SecondScreen extends StatefulWidget {
  final int index;

  SecondScreen({this.index});
  _SecondScreen createState() => _SecondScreen();
}

class _SecondScreen extends State<SecondScreen> {
  bool isTap = false;

  Icon activeIcon() {
    return Icon(
      Icons.favorite,
      color: Colors.pink,
    );
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => SwipeVendor(),
          )),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey.shade300),
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(10),
              child: Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isTap = !isTap;
                    });
                  },
                  child: isTap ? activeIcon() : Icon(Icons.favorite_outline),
                ),
              ),
            ),
            SizedBox(height: 5),
            Container(
              //width: 100,
              height: 150,
              child: Align(
                alignment: Alignment.center,
                child: Image.asset("assets/imgs/ess-removebg-preview.png"),
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Text(
                "Jean Paul Gautier",
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MerriweatherR",
                    ),
              ), //Marque du Produit
            ),
            SizedBox(height: 5),
            //Description du Produit
            SizedBox(height: 5),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Text("Taille : 36",
                  style:
                      TextStyle(fontFamily: "Merriweather") // Taille du Produit
                  ),
            ),
            SizedBox(height: 5),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Text(
                "150\$",
                style: TextStyle(
                  color: Colors.pinkAccent,
                  fontFamily: "Merriweather",
                ), //Prix du Produit
              ),
            )
          ],
        ),
      ),
    );
  }
}
