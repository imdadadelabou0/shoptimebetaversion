import 'package:flutter/material.dart';
import 'package:shoptime/screens/generalScreen/AddCartOrChat.dart';
import 'package:shoptime/screens/generalScreen/ClientDetailProduct.dart';
import 'package:shoptime/screens/generalScreen/aboutVendeur.dart';
import 'package:swipe_up/swipe_up.dart';

class SwipeVendor extends StatefulWidget {
  SwipeVendor({Key key}) : super(key: key);

  @override
  _SwipeVendorState createState() => _SwipeVendorState();
}

class _SwipeVendorState extends State<SwipeVendor> {
  @override
  Widget build(BuildContext context) {
    return SwipeUp(
      color: Colors.pink,
      sensitivity: 0.5,
      onSwipe: () {
        _settingModalBottomSheet(context);
      },
      body: ClientDetailProduct(),
      child: Material(
        color: Colors.transparent,
        child: Text(
          "swipe up",
          style: TextStyle(
            color: Colors.pink,
          ),
        ),
      ),
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
            height: (MediaQuery.of(context).size.height / 4),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.pink,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                topRight: Radius.circular(20.0),
              ),
            ),
            child: AboutVendeur(),
          );
        });
  }
}
