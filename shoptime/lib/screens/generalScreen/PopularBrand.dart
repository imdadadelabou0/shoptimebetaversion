import 'package:flutter/material.dart';

class PopularBrand extends StatefulWidget {
  PopularBrand({Key key}) : super(key: key);

  @override
  _PopularBrandState createState() => _PopularBrandState();
}

class _PopularBrandState extends State<PopularBrand> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: 5,
      itemBuilder: (context, index) {
        return Container(
          width: 135,
          child: Card(
            child: Column(
              children: [
                Expanded(
                  flex: 4,
                  child: Center(
                    child: Container(
                      color: Colors.grey.shade200,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Center(
                    child: Text(
                      "Fendi",
                      style: TextStyle(
                        fontFamily: "Merriweather",
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
