import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AboutClient extends StatefulWidget {
  AboutClient({Key key}) : super(key: key);

  @override
  _AboutClientState createState() => _AboutClientState();
}

class _AboutClientState extends State<AboutClient> {
  List<String> favoris = ["Mes Achats", "Ma wishlist", "Mes coups de coeur"];
  List<String> myCompte = [
    "Mes informations",
    "Mes informations de facturation",
    "Mes adresses de livraison",
    "Mes moyens de paiement"
  ];
  List<String> reglage = [
    "Notifications push",
    "Abonnements",
    "Confidentialité",
    "Parrainage"
  ];
  List<String> aide = [
    "Notre commission",
    "FAQ",
    "Chattez avec nous",
  ];
  double bottom = 18.0;
  final double sizeBeetweenCat = 0.0;
  final String medalIcon = "assets/imgs/medal.svg";

  Widget titleCategorie(String title) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
      child: Text(
        "$title",
        style: Theme.of(context).textTheme.headline6.copyWith(
              fontFamily: "Merriweather",
              fontWeight: FontWeight.bold,
            ),
      ),
    );
  }

  Widget containerElement(List<String> listItem) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 10.0),
      decoration: BoxDecoration(
        color: Colors.white70,
        border: Border(
          top: BorderSide(color: Colors.grey.shade300),
          bottom: BorderSide(color: Colors.grey.shade300),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(listItem.length, (index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: InkWell(
                  onTap: () => print("ok"),
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 10.0, left: 38.0, bottom: 10.0),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        listItem[index],
                        style: TextStyle(
                          fontFamily: "MerriweatherR",
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              index == listItem.length - 1
                  ? Opacity(
                      opacity: 0.0,
                      child: Divider(
                        height: 10.0,
                      ),
                    )
                  : Divider(),
            ],
          );
        }),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      /*appBar: AppBar(
        backgroundColor: Colors.white,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Icon(
            Icons.chevron_left,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        title: Text(
          "Me",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontFamily: "Merriweather",
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
        elevation: 0.0,
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 6.0),
            child: Icon(
              Icons.shopping_basket_sharp,
              color: Colors.black,
            ),
          ),
        ],
      ),*/
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white70,
                border: Border(
                  bottom: BorderSide(color: Colors.grey.shade300),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 18.0, left: 20, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            CircleAvatar(
                              radius: 39.0,
                              backgroundColor: Colors.grey,
                            ),
                            SizedBox(width: 20.0),
                            Text(
                              "Imdad",
                              style: TextStyle(
                                fontFamily: "MerriweatherR",
                              ),
                            ),
                          ],
                        ),
                        FlatButton.icon(
                          onPressed: () => print("ok"),
                          icon: SvgPicture.asset(
                            medalIcon,
                            width: 30,
                            height: 30,
                            semanticsLabel: "label",
                          ),
                          label: Text(
                            "Stats",
                            style:
                                Theme.of(context).textTheme.headline6.copyWith(
                                      fontFamily: "Merriweather",
                                      fontWeight: FontWeight.bold,
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
            titleCategorie("Mes favoris"),
            containerElement(favoris),
            titleCategorie("Mon compte"),
            containerElement(myCompte),
            titleCategorie("Réglages"),
            containerElement(reglage),
            titleCategorie("Aide"),
            containerElement(aide),
            SizedBox(height: 28.0),
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: Colors.grey.shade300),
                  bottom: BorderSide(color: Colors.grey.shade300),
                ),
              ),
              child: FlatButton(
                onPressed: () {
                  print("deconnexion");
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      "Deconnexion",
                      style: TextStyle(
                        fontFamily: "MerriweatherR",
                        color: Colors.pink,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
