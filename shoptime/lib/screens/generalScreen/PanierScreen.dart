import 'package:flutter/material.dart';
import 'package:shoptime/Component/ProductInPanier.dart';

class PanierScreen extends StatefulWidget {
  _PanierScreen createState() => _PanierScreen();
}

class _PanierScreen extends State<PanierScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Icon(
            Icons.chevron_left,
            color: Colors.black,
          ),
        ),
        title: Text(
          "Panier",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        elevation: 2.0,
        backgroundColor: Colors.white,
      ),
      body: ProductInPanier(
        imageVendeur: "assets/imgs/vendeurIcone.png",
        usernameVendeur: "username229",
        imageProduct:
            "https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/i1-9cfea66d-b519-4b29-8e43-ce4164e8c558/adapt-bb-2-tie-dye-basketball-shoe-vdFwKS.jpg",
        nameProduct: "Shoes BY Nike",
        minDescrp: "Colors Black",
        price: "100",
      ),
      backgroundColor: Colors.white.withOpacity(0.9),
    );
  }
}
