import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:shoptime/screens/generalScreen/CarouselImage.dart';

class ViewClientProductDetail extends StatefulWidget {
  _ViewClientProductDetail createState() => _ViewClientProductDetail();
}

class _ViewClientProductDetail extends State<ViewClientProductDetail> {
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              expandedHeight: 300.0,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                background: CarouselImage(),
              ),
              backgroundColor: Colors.white,
              elevation: 0.0,
            ),
            SliverFillRemaining(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 15.0, left: 15.0),
                    child: Text("ADIDAS",
                        style: TextStyle(
                          fontFamily: "BebasNeue",
                          fontSize: 50,
                          letterSpacing: 2.0,
                        )),
                  ), //Marque du Produit
                  SizedBox(
                    height: 4.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0),
                    child: Text(
                      "T-shirts",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ), // Mini Catégories du Produit
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 4 * .1,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 2 * .3,
                    //color: Colors.cyan,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 10.0, left: 15.0),
                          child: Text(
                            "36\$",
                            style:
                                Theme.of(context).textTheme.bodyText1.copyWith(
                                      color: Colors.deepOrange,
                                      fontSize: 25,
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            /*SliverList(
              delegate: SliverChildListDelegate(
                getList(),
              ),
            ),*/
            /*SliverList(delegate: SliverChildListDelegate(
            
            ))*/
          ],
        ),
      ),
    );
  }
}
