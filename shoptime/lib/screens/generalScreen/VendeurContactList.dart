import 'package:flutter/material.dart';
import 'package:shoptime/Component/ContactVendeur.dart';
import 'package:shoptime/Component/MyAppBar.dart';
import 'package:shoptime/models/User.dart';
import 'package:shoptime/services/StockUserData.dart';

class VendeurContactList extends StatefulWidget {
  _VendeurContactList createState() => _VendeurContactList();
}

class _VendeurContactList extends State<VendeurContactList> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          color: Colors.black,
          onPressed: () {},
        ),
        title: Text(
          "Contacts",
          style: TextStyle(
            color: Colors.black,
            fontFamily: "MerriweatherR",
          ),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.black,
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.white,
      ),
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          StreamBuilder(
            stream: StockUserData().getListUsers(),
            builder: (context, AsyncSnapshot<List<Users>> snapshot) {
              var vendeurList;
              if (snapshot.hasData) {
                vendeurList = snapshot.data;
                vendeurList = vendeurList
                    .where((element) => element.userType == "vendeur")
                    .toList();
              }
              return !snapshot.hasData
                  ? Text("Is Loading")
                  : Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: ListView.builder(
                          itemCount: vendeurList.length,
                          itemBuilder: (context, index) {
                            return ContactVendeur(
                              pathImage: "assets/imgs/vendeurIcone.png",
                              username: vendeurList[index].username,
                              status: "La vie qu'on mène",
                              index: index,
                              lengthList: vendeurList.length - 1,
                            );
                          },
                        ),
                      ),
                    );
            },
          ),
        ],
      ),
    );
  }
}
