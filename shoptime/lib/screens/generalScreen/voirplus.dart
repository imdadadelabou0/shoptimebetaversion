import 'package:flutter/material.dart';

class VoirPlus extends StatelessWidget {
  const VoirPlus({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(14.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          elevation: 0.0,
          onPressed: () {},
          color: Colors.black,
          child: Text(
            "Voir plus",
            style: TextStyle(
              color: Colors.white,
              fontFamily: "Merriweather",
            ),
          ),
        ),
      ),
    );
  }
}
