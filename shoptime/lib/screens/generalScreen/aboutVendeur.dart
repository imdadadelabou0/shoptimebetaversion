import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:shoptime/screens/screen1/Homescreen.dart';

class AboutVendeur extends StatefulWidget {
  AboutVendeur({Key key}) : super(key: key);

  @override
  _AboutVendeurState createState() => _AboutVendeurState();
}

class _AboutVendeurState extends State<AboutVendeur> {
  bool follow = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              "Vendeur",
              style: TextStyle(
                fontFamily: "MerriweatherR",
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
                color: Colors.white,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "OnlineShop229",
                      style: TextStyle(
                        fontFamily: "Merriweather",
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "Nouveaux",
                      style: TextStyle(
                        fontFamily: "Merriweather",
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "25 articles au Total",
                      style: TextStyle(
                        fontFamily: "Merriweather",
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                InkWell(
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => HomeScreen(),
                    ),
                  ),
                  child: CircleAvatar(
                    backgroundColor: Colors.grey,
                    backgroundImage: AssetImage("assets/imgs/imo.jpeg"),
                    radius: 39.0,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 8.0),
          Expanded(
            child: Align(
              alignment: Alignment.bottomRight,
              child: FlatButton.icon(
                onPressed: () {},
                icon: follow == false ? Icon(Icons.add) : Icon(Icons.verified),
                label: follow == false ? Text("suivre") : Text("suivi"),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
