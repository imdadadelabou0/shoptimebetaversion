class IconBottom {
  final String path;
  final String label;

  IconBottom({this.path, this.label});
}

List<IconBottom> appBar = [
  IconBottom(path: "assets/imgs/icons8-dog-house-50.png", label: "Accueil"),
  IconBottom(
      path: "assets/imgs/icons8-reminders-50.png", label: "Fil d'Actualité"),
  IconBottom(path: "assets/imgs/icons8-scooter-50.png", label: "Delivery"),
  IconBottom(
      path: "assets/imgs/icons8-notification-50.png", label: "Notifications"),
  IconBottom(path: "assets/imgs/icons8-customer-64.png", label: "Me"),
];
