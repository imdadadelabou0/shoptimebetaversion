import 'package:flutter/material.dart';
import 'package:shoptime/screens/generalScreen/ListOfNotification.dart';

class NotificationWidget extends StatefulWidget {
  _NotificationWidget createState() => _NotificationWidget();
}

class _NotificationWidget extends State<NotificationWidget>
    with SingleTickerProviderStateMixin {
  int index = 0;
  AnimationController _controller;
  Animation<Offset> offsetAnimation;
  Animation<Offset> offset2;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: false);
    offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: Offset(2.5, 0.0),
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeIn,
      ),
    );
    offset2 = Tween<Offset>(
      begin: Offset(-1.5, 0.0),
      end: Offset.zero,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeIn,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(8.0),
          margin: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    index = 0;
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  decoration: index == 0
                      ? BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(30.0),
                        )
                      : null,
                  child: Text("Messages"),
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    index = 1;
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  decoration: index == 1
                      ? BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(30.0),
                        )
                      : null,
                  child: Text("Notifications"),
                ),
              ),
            ],
          ),
        ),
        /* SlideTransition(
          position: offsetAnimation,
          child: listOfNotification[index],
        ),*/
        listOfNotification[index],
      ],
    );
  }
}
