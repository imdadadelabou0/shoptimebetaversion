import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/provider/listOrder.dart';
import 'package:shoptime/services/kkiapay.dart';
import 'package:shoptime/services/order_model.dart';
import 'package:shoptime/screens/screen3/orderView1.dart';

class OderScreen extends StatefulWidget {
  _OderScreen createState() => _OderScreen();
}

class _OderScreen extends State<OderScreen> {
  MyKKiapay mkKiapay = MyKKiapay();
  void successCallback(response, context) {
    Navigator.pop(context);
    /* Navigator.push(
    context,
    MaterialPageRoute(
        builder: (context) => SuccessScreen( 
              amount: response['amount'],
              transactionId: response['transactionId']
            )),
  );*/
  }

  @override
  Widget build(context) {
    final kkiapay = mkKiapay.initKKiapay(
        callback: successCallback,
        amount: 1000,
        sandbox: false,
        data: 'fakeData',
        apiKey: '8FntFEaLFHqyL1g2jN5d8HPBAl1hsT',
        phone: '67558797',
        theme: '#2ba359');
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.chevron_left, color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 13.0),
            child: Text(
              "My Order",
              style: Theme.of(context).textTheme.headline6.copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
            ),
          ),
          Expanded(
            child: OrderView1(),
          ),
          Container(
            padding: EdgeInsets.all(15.0),
            width: double.infinity,
            child: RaisedButton(
              color: Colors.green,
              child: Text(
                "Confirm Payment",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: Colors.white),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              onPressed: () => Navigator.push(
                  context, MaterialPageRoute(builder: (context) => kkiapay)),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }
}
