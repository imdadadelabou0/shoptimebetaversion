import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/models/orderProduct.dart';
import 'package:shoptime/provider/listOrder.dart';
import 'package:shoptime/services/DataBaseOrder.dart';

class OrderView1 extends StatefulWidget {
  _OrderView1 createState() => _OrderView1();
}

class _OrderView1 extends State<OrderView1> {
  deleteOrder(int id, OrderProduct order, BuildContext context,
      String idProduct) async {
    Provider.of<ListOrder>(context, listen: false)
        .delateOrder(id, order, idProduct, context);
    final snack = SnackBar(content: Text("Succesful Delete !!!"));
    Scaffold.of(context).showSnackBar(snack);
  }

  Widget build(context) {
    var orderList = Provider.of<ListOrder>(context, listen: true).getListOrder;
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: orderList.length, //orderList.length,
      itemBuilder: (context, index) {
        final order = orderList[index];
        return Dismissible(
          background: Container(
            color: Colors.green,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Icon(
                Icons.check,
              ),
            ),
          ),
          secondaryBackground: Container(
            alignment: Alignment.centerRight,
            color: Colors.red,
            child: Icon(
              Icons.delete,
              color: Colors.black,
            ),
          ),
          direction: DismissDirection.endToStart,
          onDismissed: (direction) =>
              deleteOrder(order.id, order, context, order.fireId),
          key: UniqueKey(),
          child: ListTile(
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Image.network(
                order.productImage,
                width: 70,
                height: 100,
                // fit: BoxFit.fill,
              ),
            ),
            title: Align(
              alignment: Alignment.topCenter,
              child: Text(
                order.name,
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            subtitle: Text(
              "2",
              textAlign: TextAlign.center,
            ),
            trailing: Text(
              order.price,
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
        );
      },
    );
  }
}
