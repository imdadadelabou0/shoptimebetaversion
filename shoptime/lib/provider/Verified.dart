import 'package:flutter/foundation.dart';

class Verified extends ChangeNotifier {
  bool verified = false;

  bool get getVerified {
    return verified;
  }

  void updateVerified() {
    verified = true;
    notifyListeners();
  }

  void setFalse() {
    verified = false;
    notifyListeners();
  }
}
