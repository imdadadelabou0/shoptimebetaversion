import 'package:flutter/material.dart';

class ProviderEmail extends ChangeNotifier {
  String mail = '';

  String get getMail => mail;

  setMail(String newMail) {
    mail = newMail;
  }
}
