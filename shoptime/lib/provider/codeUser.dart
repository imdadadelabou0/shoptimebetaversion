import 'package:flutter/cupertino.dart';

class CodeUser extends ChangeNotifier {
  String code;

  String get getCode => code;

  updateCode(String oldcode) {
    code = oldcode;
    notifyListeners();
  }
}
