import 'dart:collection';

import 'package:flutter/material.dart';

class Active extends ChangeNotifier {
  int _active = 0;

  int get getActive {
    return _active;
  }

  void updateActive({int active}) {
    _active = active;
    notifyListeners();
  }
}
