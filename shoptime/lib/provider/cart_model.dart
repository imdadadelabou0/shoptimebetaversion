import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:collection';
import 'package:shoptime/services/sharedPreferences.dart';

class CartModel extends ChangeNotifier {
  /// Internal, private state of the cart.
  final List<String> _productIds = [];
  /* CartModel() {
    initialize();
  } */

  Future initialize() async {
    var savedIds = await SharePreferencesProduct().getIdList();
    for (var id in savedIds) {
      _productIds.add(id);
    }
  }

  /// An unmodifiable view of the items in the cart.
  /*UnmodifiableListView<String> get productIds =>
      UnmodifiableListView(_productIds);*/

  List<String> get productIds => _productIds;

  /// The current total price of all items (assuming all items cost $42).
  // int get totalPrice => _items.length * 42;

  /// Adds [productId] to cart. This and [removeAll] are the only ways to modify the
  /// cart from the outside.
  ///

  updateProductId(List<String> list) {
    // removeAll();
    list.forEach((element) {
      _productIds.add(element);
    });
    notifyListeners();
  }

  void add(String id) {
    _productIds.add(id);
    SharePreferencesProduct().setIdList(id: id);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  /// Removes all items from the cart.
  void removeAll() {
    _productIds.clear();
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  void removeId(String id) async {
    _productIds.remove(id);
    await SharePreferencesProduct().setListId(_productIds);
    notifyListeners();
  }
}
