import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/models/orderProduct.dart';
import 'package:shoptime/provider/ProductInCart.dart';
import 'package:shoptime/provider/cart_model.dart';
import 'package:shoptime/services/DataBaseOrder.dart';
import 'package:shoptime/services/sharedPreferences.dart';

class ListOrder extends ChangeNotifier {
  DataBaseOrder dbOrder = DataBaseOrder();
  final List<String> _listIds = [];
  final List<OrderProduct> _listOrder = new List();

  var dbPath;
  ListOrder() {
    initialize();
  }

  DataBaseOrder get getInstance => dbOrder;

  initialize() async {
    await dbOrder.create();
    List<String> savedId = await SharePreferencesProduct().getOrderId();
    for (var element in savedId) {
      _listIds.add(element);
    }
    for (var id in _listIds) {
      var orderProduct = await dbOrder.getOrderProduct(int.parse(id));
      _listOrder.add(orderProduct);
    }
    print(_listOrder.length);
    notifyListeners();
  }

  List<OrderProduct> get getListOrder {
    return _listOrder;
  }

  update(int id) async {
    var productOrder = await dbOrder.getOrderProduct(id);
    _listOrder.add(productOrder);
    notifyListeners();
  }

  delateOrder(int id, OrderProduct order, String idProduct,
      BuildContext context) async {
    _listOrder.remove(order);
    await dbOrder.deleteOrder(id);
    _listIds.remove(id.toString());
    Provider.of<CartModel>(context, listen: false).removeId(idProduct);
    notifyListeners();
  }

  UnmodifiableListView<String> get getOrderId => UnmodifiableListView(_listIds);

  void addId(String id) {
    _listIds.add(id);
    SharePreferencesProduct().setOrderId(id: id);
    notifyListeners();
  }
}
