import 'package:flutter/cupertino.dart';

class UserType extends ChangeNotifier {
  bool acheteur = false;
  bool vendeur = false;
  String userType = " ";

  String get getUserType => userType;

  void updateValue(bool valueAcheteur, bool valueVendeur) {
    acheteur = valueAcheteur;
    vendeur = valueVendeur;
    ifAcheteurOrVendeur(valueAcheteur, valueVendeur);
    notifyListeners();
  }

  void ifAcheteurOrVendeur(bool vAcheteur, bool vVendeur) {
    if (vAcheteur == true) {
      userType = "acheteur";
    }
    if (vVendeur == true) {
      userType = "vendeur";
    }
    notifyListeners();
  }
}
