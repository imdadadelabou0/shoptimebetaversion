import 'package:flutter/foundation.dart';

class ProductInCart extends ChangeNotifier {
  List<String> productId = [];

  List<String> get getList {
    return productId;
  }

  void addToCart({String id}) {
    productId.add(id);
    notifyListeners();
  }
}
