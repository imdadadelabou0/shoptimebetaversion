import 'package:shoptime/models/orderProduct.dart';

enum EventType { add, delete }

class OrderEvent {
  OrderProduct orderProduct;
  int orderIndex;
  EventType eventType;

  OrderEvent.add(OrderProduct order) {
    this.eventType = EventType.add;
    this.orderProduct = order;
  }

  OrderEvent.delete(int index) {
    this.eventType = EventType.delete;
    this.orderIndex = index;
  }
}
