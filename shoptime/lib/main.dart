import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/connexion/CustomLogin.dart';
import 'package:shoptime/provider/Active.dart';
import 'package:shoptime/provider/ProviderEmail.dart';
import 'package:shoptime/provider/UserType.dart';
import 'package:shoptime/provider/Verified.dart';
import 'package:shoptime/provider/codeUser.dart';
import 'package:shoptime/provider/listOrder.dart';
import 'package:shoptime/screens/generalScreen/firstScreen.dart';
import 'package:shoptime/screens/screen1/BeginScreen.dart';
import 'package:shoptime/screens/screen1/EssPage.dart';
import 'package:shoptime/screens/screen1/MyLogin.dart';
import 'package:shoptime/screens/screen1/MyTab.dart';
import 'package:shoptime/screens/screen1/RegisterEmail.dart';
import 'package:shoptime/screens/screen1/SignUp.dart';
import 'package:shoptime/screens/screen1/loginPage.dart';
import 'package:shoptime/services/FacebookAuth.dart';
import 'package:shoptime/services/order_model.dart';
import 'package:shoptime/services/firebase_auth.dart';
import 'screens/screen1/Homescreen.dart';
import 'package:shoptime/provider/cart_model.dart';
import 'package:shoptime/services/Confirmation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => Active(),
        ),
        ChangeNotifierProvider(
          create: (context) => CartModel()..initialize(),
        ),
        ChangeNotifierProvider(
          create: (context) => OrderModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => ListOrder(),
        ),
        ChangeNotifierProvider(create: (context) => Verified()),
        ChangeNotifierProvider(
          create: (context) => Firebaseauth()..userState(),
        ),
        ChangeNotifierProvider(
          create: (context) => CodeUser(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProviderEmail(),
        ),
        ChangeNotifierProvider(
          create: (context) => CustomFacebookAuth(),
        ),
        ChangeNotifierProvider(
          create: (context) => UserType(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'ShopTime',
        home: BeginScreen() /*FirstScreen()*/,
        /*CustomLogin(),*/
        routes: {
          "/EssPage": (context) => EssPage(),
          "/SignUp": (context) => SignUp(),
          "/Confirmation": (context) => Confirmation(),
          "/FirstScreen": (context) => FirstScreen(),
        }, //HomeScreen(),
      ),
    );
  }
}
