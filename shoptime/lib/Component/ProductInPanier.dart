import 'package:flutter/material.dart';

class ProductInPanier extends StatefulWidget {
  final String imageVendeur,
      usernameVendeur,
      imageProduct,
      nameProduct,
      minDescrp,
      price;
  ProductInPanier({
    this.imageVendeur,
    this.usernameVendeur,
    this.imageProduct,
    this.nameProduct,
    this.minDescrp,
    this.price,
  });
  _ProductPanier createState() => _ProductPanier();
}

class _ProductPanier extends State<ProductInPanier> {
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: Container(
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.grey,
                  backgroundImage:
                      AssetImage(widget.imageVendeur), //Avatar du Vendeur
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(widget.usernameVendeur),
              ],
            ),
            Divider(),
            Row(
              children: [
                Container(
                  width: 80,
                  height: 90,
                  child: Image.network(
                    widget.imageProduct,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.nameProduct,
                      style: TextStyle(
                        fontFamily: "MerriweatherR",
                      ),
                    ), //Nom du produit
                    Text(
                      widget.minDescrp,
                      style: TextStyle(
                        fontFamily: "Merriweather",
                      ),
                    ), // BRef description
                  ],
                ),
                Spacer(), //Nom Du Produit
                Text(
                  "${widget.price} \$",
                  style: TextStyle(
                    fontFamily: "Merriweather",
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
                InkWell(
                  onTap: () {
                    print("ok");
                  },
                  child: Image.asset(
                    "assets/imgs/dotMenu.png",
                    scale: 1.1,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
