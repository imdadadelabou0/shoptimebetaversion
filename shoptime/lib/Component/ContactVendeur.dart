import 'package:flutter/material.dart';

class ContactVendeur extends StatefulWidget {
  final String pathImage, username, status;
  final int index, lengthList;
  ContactVendeur(
      {this.pathImage,
      this.username,
      this.status,
      this.index,
      this.lengthList});

  @override
  _ContactVendeurState createState() => _ContactVendeurState();
}

class _ContactVendeurState extends State<ContactVendeur> {
  List<Widget> listElement = [];
  void initState() {
    super.initState();
    listElement.add(
      Text(
        widget.username,
        style: TextStyle(
          fontFamily: "MerriweatherR",
        ),
      ),
    );
    if (widget.status != null) {
      listElement.add(
        SizedBox(height: 8.0),
      );
      listElement.add(
        Text(
          widget.status,
          style: TextStyle(
            fontFamily: "Merriweather",
            fontSize: 10.0,
          ),
        ),
      );
    }
  }

  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 10.0,
      ),
      child: Column(
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundImage: AssetImage(widget.pathImage),
                radius: 30.0,
              ),
              SizedBox(
                width: 20.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: listElement,
              ),
            ],
          ),
          widget.index != widget.lengthList
              ? Padding(
                  padding: const EdgeInsets.only(
                    left: 80.0,
                    right: 20.0,
                  ),
                  child: Divider(),
                )
              : Opacity(
                  opacity: 0.0,
                  child: Text("ok"),
                )
        ],
      ),
    );
  }
}
