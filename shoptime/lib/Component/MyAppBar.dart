import 'package:flutter/material.dart';
import 'package:shoptime/screens/generalScreen/PanierScreen.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final int index;
  MyAppBar({this.index});
  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.black54),
      backgroundColor: Color(0xfff9f9f9),
      elevation: 0.0,
      leading: !(index == 4)
          ? IconButton(
              icon: Icon(Icons.menu),
              color: Colors.black,
              onPressed: () {},
            )
          : IconButton(
              icon: Icon(
                Icons.chevron_left,
                color: Colors.black,
              ),
              onPressed: () {},
            ),
      actions: [
        Padding(
          padding: EdgeInsets.all(3.0),
          child: Stack(
            children: [
              IconButton(
                icon: Icon(Icons.shopping_basket),
                color: Colors.black,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => PanierScreen(),
                    ),
                  );
                },
              ),
              Positioned(
                top: 0,
                right: 0,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.blue),
                    color: Colors.blue,
                  ),
                  child: Text("1"),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
