import 'package:shoptime/models/product.dart';

class OrderProduct {
  String name, description, price, category, productImage, timeStamp, fireId;
  int id;

  OrderProduct copyToOrder({Product product}) {
    OrderProduct order = OrderProduct();
    order.name = product.name;
    order.description = product.description;
    order.price = product.price;
    order.category = product.category;
    order.productImage = product.productImage;
    order.timeStamp = product.timestamp.toString();
    order.fireId = product.id;
    return order;
  }

  static final columns = [
    "id",
    "name",
    "description",
    "price",
    "category",
    "productImage",
    "timeStamp",
    "fireId"
  ];

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "name": name,
      "description": description,
      "price": price,
      "category": category,
      "productImage": productImage,
      "timeStamp": timeStamp,
      "fireId": fireId
    };
    if (id != null) {
      map["id"] = id;
    }
    return map;
  }

  static OrderProduct fromMap(Map<String, dynamic> map) {
    OrderProduct product = OrderProduct();

    product.id = map["id"];
    product.name = map["name"];
    product.description = map["description"];
    product.price = map["price"];
    product.category = map["category"];
    product.productImage = map["productImage"];
    product.timeStamp = map["timeStamp"];
    product.fireId = map["fireId"];
    return product;
  }
}
