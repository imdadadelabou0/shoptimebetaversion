class Users {
  final String username;
  final String numTelOrMail;
  final String password;
  final String userType;
  final String idUser;
  Users(
      {this.username,
      this.numTelOrMail,
      this.password,
      this.userType,
      this.idUser});

  Map<String, dynamic> userToJson() {
    return {
      "username": username,
      "numTelOrMail": numTelOrMail,
      "password": password,
      "userType": userType,
      "idUser": idUser,
    };
  }

  Users fromJsonToUser(Map<String, dynamic> json) {
    return Users(
      username: json['username'],
      numTelOrMail: json['numTelOrMail'],
      password: json['password'],
      userType: json['userType'],
      idUser: json['idUser'],
    );
  }
}
