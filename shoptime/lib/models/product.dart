import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  final String id;
  final String name;
  final String description;
  final String price;
  final String category;
  final String productImage;
  final Timestamp timestamp;

  const Product(
      {this.name = '',
      this.description = '',
      this.price = '',
      this.category = '',
      this.productImage = '',
      this.id,
      this.timestamp});

  @override
  String toString() {
    return 'Product(id: $id, name: $name, description: $description, price: $price, category: $category, productImage: $productImage)';
  }

  // Return a JSON representation of this object
  // JSON is needed when creating a firestore document
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'price': price,
      'category': category,
      'productImage': productImage,
      'timestamp': Timestamp.now()
    };
  }

  // Returns a copy of this object while altering the given params
  Product copyWith(
      {String id,
      String name,
      String description,
      String price,
      String category,
      String productImage,
      Timestamp timestamp}) {
    return Product(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        price: price ?? this.price,
        category: category ?? this.category,
        productImage: productImage ?? this.productImage,
        timestamp: timestamp ?? this.timestamp);
  }

  // Return this object from a JSON
  Product fromJson(Map<String, dynamic> json) {
    return Product(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        price: json['price'],
        category: json['category'],
        productImage: json['productImage'],
        timestamp: json['timestamp']);
  }
}
