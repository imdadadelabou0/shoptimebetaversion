class SigninUser {
  final String connectionId;
  final String password;
  SigninUser({this.connectionId, this.password});
}
