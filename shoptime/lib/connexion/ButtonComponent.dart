import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/connexion/PageEss.dart';
import 'package:shoptime/services/FacebookAuth.dart';

class ButtonComponent extends StatelessWidget {
  final String text;
  final Color couleur, buttonColor;
  final Function() ifPressed;
  ButtonComponent({this.text, this.couleur, this.buttonColor, this.ifPressed});
  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0))),
      child: Container(
        width: MediaQuery.of(context).size.width * .7 + 12,
        height: 50,
        child: RaisedButton(
          color: buttonColor != null ? buttonColor : null,
          onPressed: ifPressed,
          child: Text(
            text,
            style: TextStyle(
              color: couleur,
              fontFamily: "MerriweatherR",
            ),
          ),
        ),
      ),
    );
  }
}

class SignInWithFb extends StatelessWidget {
  final String label;
  final Color couleur;
  const SignInWithFb({Key key, this.label, this.couleur}) : super(key: key);

  ifUserCompleteConnection(BuildContext context) {
    if (context.read<CustomFacebookAuth>().userProfile != null) {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PageEss(),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0))),
      child: Builder(
        builder: (ctx) => Container(
          width: MediaQuery.of(ctx).size.width * .7 + 12,
          height: 50,
          child: RaisedButton.icon(
            onPressed: () async {
              await Provider.of<CustomFacebookAuth>(context, listen: false)
                  .initiateFacebookLogin()
                  .then((value) {
                ifUserCompleteConnection(context);
              });
            },
            icon: SvgPicture.asset(
              "assets/imgs/facebookicone.svg",
              width: 50,
            ),
            label: Text(
              label,
              style: TextStyle(
                color: couleur,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SignUpWithFacebook extends StatelessWidget {
  const SignUpWithFacebook({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0))),
      child: Builder(
        builder: (ctx) => Container(
          width: MediaQuery.of(ctx).size.width * .7 + 12,
          height: 50,
          child: RaisedButton.icon(
            onPressed: () async {},
            icon: SvgPicture.asset(
              "assets/imgs/facebookicone.svg",
              width: 50,
            ),
            label: Text(
              "s'inscrire avec facebook",
              style:
                  TextStyle(color: Colors.white, fontFamily: "MerriweatherR"),
            ),
          ),
        ),
      ),
    );
  }
}
