import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/connexion/ButtonComponent.dart';
import 'package:shoptime/connexion/FormFieldComponent.dart';
import 'package:shoptime/models/SigninUser.dart';
import 'package:shoptime/services/firebase_auth.dart';

import 'MainText.dart';

class CustomLogin extends StatefulWidget {
  CustomLogin({Key key}) : super(key: key);

  @override
  _CustomLoginState createState() => _CustomLoginState();
}

class _CustomLoginState extends State<CustomLogin> {
  String identifiant = " ";
  String password = " ";
  bool ifcheck = false;
  final _formKey = GlobalKey<FormState>();

  Future<Null> validatorSignIn(context, errorCode, username) async {
    if (errorCode == "user-not-found") {
      return _showDialog(
          context, "Username $username not exist.", "", errorCode);
    } else if (errorCode == "wrong-password") {
      return _showDialog(
        context,
        "Incorrect password",
        "The password you entered is incorrect. Please try again.",
        errorCode,
      );
    }
  }

  Future<Null> _showDialog(
    BuildContext context,
    String errorText,
    String user,
    String errorAuth,
  ) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  errorText,
                  style: errorAuth == "wrong-password"
                      ? Theme.of(context).textTheme.headline6
                      : Theme.of(context).textTheme.bodyText1,
                ),
                SizedBox(height: 5.0),
                Text(
                  user,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .apply(color: Colors.grey),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Divider(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Text(
                      "Retry",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    bool onTap = false;
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.5),
                ),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.only(left: 50.0),
                    child: MainText(
                      text: "Connectez-vous à \nvotre compte",
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 40,
                        ),
                        FormFieldComponent(
                          labelText: "Téléphone ou e-mail",
                          change: (value) {
                            setState(() {
                              identifiant = value;
                            });
                          },
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        FormFieldComponent(
                            labelText: "Mot de passe",
                            change: (value) {
                              setState(() {
                                password = value;
                              });
                            }),
                        SizedBox(
                          height: 25.0,
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 50.0),
                          child: Align(
                            alignment: Alignment.topRight,
                            child: InkWell(
                              onTap: () {
                                print("ok");
                              },
                              child: Text(
                                "Mot de passe oublié?",
                                style: TextStyle(
                                  color: Colors.pink,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 35),
                          child: Row(
                            children: [
                              Checkbox(
                                onChanged: (value) {
                                  setState(() {
                                    ifcheck = value;
                                  });
                                },
                                value: ifcheck,
                                activeColor: Colors.pink,
                              ),
                              Text(
                                "Se souvenir de moi",
                                style: TextStyle(
                                  fontFamily: "Merriweather",
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 25.0,
                        ),
                        ButtonTheme(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0))),
                          child: Container(
                            width: MediaQuery.of(context).size.width * .7 + 12,
                            height: 50,
                            child: RaisedButton(
                                color: Colors.pink,
                                child: Text(
                                  "Connexion",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "MerriweatherR",
                                  ),
                                ),
                                onPressed: () async {
                                  if (_formKey.currentState.validate()) {
                                    await Provider.of<Firebaseauth>(context,
                                            listen: false)
                                        .authSignIn(
                                      user: SigninUser(
                                        connectionId: identifiant,
                                        password: password,
                                      ),
                                    )
                                        .then((value) {
                                      print(value.user.email);
                                      if (onTap == false) {
                                        setState(() {
                                          onTap = true;
                                        });
                                        Navigator.pushNamed(
                                            context, "/EssPage");
                                      }
                                    }).catchError((e) {
                                      if (onTap == false) {
                                        setState(() {
                                          onTap = true;
                                        });
                                        validatorSignIn(
                                            context, e.code, e.email);
                                      }
                                    });
                                  }
                                }),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        SignInWithFb(
                          label: "se connecter avec facebook",
                          couleur: Colors.white,
                        ),
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
