import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_field_validator/form_field_validator.dart';

class FormFieldComponent extends StatefulWidget {
  final String labelText, typeText;
  final Function(String value) change;
  final TextInputType typeKeyboard;
  FormFieldComponent(
      {Key key, this.labelText, this.change, this.typeKeyboard, this.typeText})
      : super(key: key);

  @override
  _FormFieldComponentState createState() => _FormFieldComponentState();
}

class _FormFieldComponentState extends State<FormFieldComponent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(left: 50.0, right: 50.0),
        child: TextFormField(
          decoration: InputDecoration(
            border: UnderlineInputBorder(),
            labelText: widget.labelText,
            labelStyle: TextStyle(fontFamily: "Merriweather"),
          ),
          validator: RequiredValidator(
            errorText: "Le champ est obligatoire",
          ),
          onChanged: widget.change,
          keyboardType:
              widget.typeKeyboard != null ? widget.typeKeyboard : null,
        ),
      ),
    );
  }
}

class Passwordfield extends StatefulWidget {
  final Function(String value) change;
  Passwordfield({this.change});

  @override
  _PasswordfieldState createState() => _PasswordfieldState();
}

class _PasswordfieldState extends State<Passwordfield> {
  final dataValidator = MultiValidator([
    RequiredValidator(errorText: "Le champ est obligatoire"),
    MinLengthValidator(8,
        errorText: "Le mot de passe doit comporter au moins 8 caractères"),
  ]);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(left: 50.0, right: 50.0),
        child: TextFormField(
          decoration: InputDecoration(
            border: UnderlineInputBorder(),
            labelText: "Mot de passe",
            labelStyle: TextStyle(
              fontFamily: "Merriweather",
            ),
          ),
          validator: dataValidator,
          onChanged: widget.change,
        ),
      ),
    );
  }
}
