import 'package:flutter/material.dart';

class MainText extends StatelessWidget {
  final String text;
  const MainText({Key key, this.text}) : super(key: key);

  String addRetour(String text) {
    var textList = text.split(" ");
    String newText = "";
    if (textList.length >= 3) {
      newText = textList[0] + " " + textList[1];
      for (int i = 0; i < textList.length; i++) {
        if (!(i >= 0 && i <= 1)) newText += " " + textList[i];
      }
    } else {
      newText = text;
    }
    return newText;
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      addRetour(text),
      //"Connectez-vous à \nvotre compte",
      style: TextStyle(
        fontSize: 20.0,
        fontFamily: "Merriweather",
      ),
    );
  }
}
