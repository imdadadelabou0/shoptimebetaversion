import 'package:flutter/material.dart';

class CheckBox extends StatefulWidget {
  final String text;
  CheckBox({Key key, this.text}) : super(key: key);

  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool ifcheck = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 35),
      child: Row(
        children: [
          Checkbox(
            onChanged: (value) {
              setState(() {
                ifcheck = value;
              });
            },
            value: ifcheck,
            activeColor: Colors.pink,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.text != null ? widget.text : " ",
                style: TextStyle(
                  fontFamily: "Merriweather",
                  fontSize: 10.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
