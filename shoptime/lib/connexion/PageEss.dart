import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:provider/provider.dart';
import 'package:shoptime/services/FacebookAuth.dart';

class PageEss extends StatelessWidget {
  const PageEss({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var userProfile = context.watch<CustomFacebookAuth>().userProfile;
    return Scaffold(
      body:
          Consumer<CustomFacebookAuth>(builder: (context, facebookAuth, child) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage(
                userProfile["picture"]["data"]["url"],
              ),
            ),
            Text(userProfile["name"]),
            Spacer(),
            Center(
              child: RaisedButton(
                onPressed: () async {
                  await context.read<CustomFacebookAuth>().logOut();
                  Navigator.of(context).pop();
                },
                child: Text("Logout"),
              ),
            ),
          ],
        );
      }),
    );
  }
}
